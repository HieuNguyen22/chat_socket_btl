/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author unin
 */
public class Messages implements Serializable {
    private List<MessageModel> listmessages;

    public Messages() {
    }
    

    public Messages(List<MessageModel> listmessages) {
        this.listmessages = listmessages;
    }

    public List<MessageModel> getListmessages() {
        return listmessages;
    }

    public void setListmessages(List<MessageModel> listmessages) {
        this.listmessages = listmessages;
    }
    
    
}
