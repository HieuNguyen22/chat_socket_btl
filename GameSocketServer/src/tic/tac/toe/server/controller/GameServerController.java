/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tic.tac.toe.server.controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import utils.Constants;

/**
 *
 * @author 84978
 */
public class GameServerController {
    
    private int sessionNo = 0;
    
    public List<GameServerClientController> sockerClients = new ArrayList<>();
    public List<GameServerClientController> playingClients = new ArrayList<>();
    public ServerSocket serverSocket;
    public JFrame frame;
    public JTextArea logs;
    public JTextField textNumMatch;
    public JTextField textNumClient;
    
    private GameServerController self = this;
    
    public GameServerController(JFrame frame, JTextArea logs, JTextField textNumMatch, JTextField textNumClient) {
        self = this;
        sockerClients = new ArrayList<>();
        playingClients = new ArrayList<>();
        this.logs = logs;
        this.frame = frame;
        this.textNumClient = textNumClient;
        this.textNumMatch = textNumMatch;
        
        startServer();
    }
    
    public String startServer() {
        try {
            // Create a server socket
            serverSocket = new ServerSocket(Constants.SERVER_PORT);
            
            // Connect to client
            new Thread(new ClientRegistration()).start();

            String log = new Date() + ": Server started at socket " + Constants.SERVER_PORT;
            logs.append(log + "\n");
            return log; 
        } catch (IOException ex) {
            Logger.getLogger(GameServerController.class.getName()).log(Level.SEVERE, null, ex);
            String log = new Date() + ": Start server game error!!!";
            logs.append(log + "\n");
            return log;
        }   
    }
    
    public boolean startGame(GameServerClientController socketClient1, GameServerClientController socketClient2) {
        if(playingClients.contains(socketClient1) || playingClients.contains(socketClient2)) {
            return true;
        }
        
        playingClients.add(socketClient1);
        playingClients.add(socketClient2);
        String log = new Date() + 
                ": Start a game for client1: " + 
                socketClient1.getClient().getInetAddress().getHostAddress() + "/" + socketClient1.getUser().getTenDangNhap() +
                ", client2: " + socketClient2.getClient().getInetAddress().getHostAddress()  + "/" + socketClient2.getUser().getTenDangNhap() ;
        
        logs.append(log + "\n");
        
        new Thread(new HandleSessionGame(socketClient1, socketClient2)).start();
        
        return false;
    }
    
    public boolean stop() throws IOException {
        serverSocket.close();
        String log = new Date() + ": Server stoped at socket " + Constants.SERVER_PORT;
        logs.append(log);
        this.frame.dispose();
        return true;
    }
    
    public long getMatch() {
        return sessionNo;
    }

    public GameServerClientController getUserSocketByUserName(String username) {
        List<GameServerClientController> clientControllers = sockerClients.stream()
                .filter(c -> c.getUser() != null && c.getUser().getTenDangNhap().equals(username))
                .collect(Collectors.toList());
        
        if(clientControllers == null || clientControllers.isEmpty()) {
            return null;
        } else {
            return clientControllers.get(0);
        }
    }
    
    public List<String> getOtherPeople(String usernameRequest) {
        return sockerClients.stream()
            .filter(c -> c.getUser() != null).map(c -> c.getUser().getTenDangNhap())
            .filter(username -> !username.equals(usernameRequest)).collect(Collectors.toList());
    }

    public void unPlaying(String username) {
        GameServerClientController clientController = getUserSocketByUserName(username);
        playingClients.remove(clientController);
    }
    
    private class ClientRegistration implements Runnable {

        @Override
        public void run() {
            // Connect to client
            while(true) {
                try {
                    Socket client = serverSocket.accept();
                    self.sessionNo += 1;
                    String log = new Date() + ": Start a thread for client: " + 
                                    client.getInetAddress().getHostAddress() + " - sessionNo: " + sessionNo;
                    logs.append(log + "\n");
                    textNumClient.setText(Integer.parseInt(textNumClient.getText()) + 1 + "");
                   
                    GameServerClientController gameServerClientController = new GameServerClientController(self, client, self.sessionNo);
                    sockerClients.add(gameServerClientController);
                    new Thread(gameServerClientController).start();
                } catch (IOException ex) {
                    Logger.getLogger(GameServerController.class.getName()).log(Level.SEVERE, null, ex);
                    String log = new Date() + ": Start a thread for client error!!!";
                    logs.append(log + "\n");
                }
            }
        }
        
    }
    
    public static void main(String[] args) {
    }
}
