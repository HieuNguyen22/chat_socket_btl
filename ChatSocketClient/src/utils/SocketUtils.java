/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author 84978
 */
public class SocketUtils {
    public static String readUTF(ObjectInputStream is) throws IOException {
        return is.readUTF();
    }
      
    public static Object read(ObjectInputStream is) throws IOException, ClassNotFoundException {
        return is.readObject();
    }
    
    public static void writeUTF(ObjectOutputStream os, String str) throws IOException  {
        os.writeUTF(str);
        os.flush();
    }
    
    public static void write(ObjectOutputStream os, Object o) throws IOException  {
        os.writeObject(o);
        os.flush();
    }
    
    public static void writeInt(DataOutputStream os, int num) throws IOException  {
        os.writeInt(num);
        os.flush();
    }
    
    public static int readInt(DataInputStream is) throws IOException {
        return is.readInt();
    }
}
