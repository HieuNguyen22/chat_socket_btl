/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tic.tac.toe.dao;

import java.sql.*;

/**
 *
 * @author 84978
 */
public class DBConnection {
    
    public static String host = "101.99.6.31";
    public static int port = 11122;
    public static String database = "ltm_chatapp";
    public static String username = "ltm";
    public static String password = "Aladin@123";

    public static Connection connection;
    
    public static Connection getInstant() {
        if(connection == null) {
            connection = getConnection();
        }
        
        return connection;
    }
    
    public static Connection getConnection() {
        try {
            String url
                = "jdbc:mysql://" + host + ":" + port + "/" + database;
            Class.forName("com.mysql.cj.jdbc.Driver"); 
            return DriverManager.getConnection(url, username, password);
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
