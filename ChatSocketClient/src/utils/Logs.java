/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package utils;

import java.util.Date;

/**
 *
 * @author 84978
 */
public class Logs {
    public static void log(String message) {
        System.out.println(new Date() + ": " + message);
    }
}
