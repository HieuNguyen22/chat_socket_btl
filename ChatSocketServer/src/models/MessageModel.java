/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Avada Kedavra
 */
public class MessageModel implements Serializable{
    private int id;
    private int senderId;
    private int receiverId;
    private String text;
    private String timeCreated;
    private int isGroup;
    private int isIcon;
    private int isFile;

    public MessageModel(int id, int senderId, int receiverId, String text, String timeCreated, int isGroup, int isIcon) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.text = text;
        this.timeCreated = timeCreated;
        this.isGroup = isGroup;
        this.isIcon = isIcon;
        this.isFile = 0;
    }

    public MessageModel(int id, int senderId, int receiverId, String text, String timeCreated, int isGroup, int isIcon, int isFile) {
        this.id = id;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.text = text;
        this.timeCreated = timeCreated;
        this.isGroup = isGroup;
        this.isIcon = isIcon;
        this.isFile = isFile;
    }

    public int getIsFile() {
        return isFile;
    }

    public void setIsFile(int isFile) {
        this.isFile = isFile;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public int getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(int receiverId) {
        this.receiverId = receiverId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public int getIsGroup() {
        return isGroup;
    }

    public void setIsGroup(int isGroup) {
        this.isGroup = isGroup;
    }

    public int getIsIcon() {
        return isIcon;
    }

    public void setIsIcon(int isIcon) {
        this.isIcon = isIcon;
    }

 
    
}
