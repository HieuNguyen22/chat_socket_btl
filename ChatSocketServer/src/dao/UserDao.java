/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import connection.ConnectionUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import models.UserModel;

public class UserDao {
    
    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        System.out.println(getUsers().size());
    }

    public static List<UserModel> getUsers() throws SQLException, ClassNotFoundException {
        List<UserModel> users = new ArrayList<>();
        
        // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sql = "Select * from user";

        // Thực thi câu lệnh SQL trả về đối tượng ResultSet.
        ResultSet rs = statement.executeQuery(sql);

        // Duyệt trên kết quả trả về.
        while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
            int id = rs.getInt(1);
            String username = rs.getString(2);
            String password = rs.getString("password");
            String fullname = rs.getString("fullname");
            String checkOnline = rs.getString("checkOnline");
            users.add(new UserModel(id, username, password, fullname, checkOnline));

        }
        // Đóng kết nối
        connection.close();
        return users;
    }
}
