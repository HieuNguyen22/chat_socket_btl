/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Avada Kedavra
 */
public class UserModel implements Serializable{
    private int id;
    private String username;
    private String password;
    private String fullname;
    private String checkOnline;
    public UserModel() {
    }

    public UserModel(int id, String username, String password, String fullname, String checkOnline) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.fullname = fullname;
        this.checkOnline = checkOnline;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    public String getCheckOnline() {
        return checkOnline;
    }

    public void setCheckOnline(String checkOnline) {
        this.checkOnline = checkOnline;
    }
    @Override
    public String toString() {
        return "UserModel{" + "id=" + id + ", username=" + username + ", password=" + password + ", fullname=" + fullname + ", checkOnline=" + checkOnline +'}';
    }

    
}
