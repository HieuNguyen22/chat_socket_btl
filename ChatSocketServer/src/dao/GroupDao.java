/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import connection.ConnectionUtils;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import models.GroupModel;
import models.ParticipantModel;
import models.UserModel;

/**
 *
 * @author Avada Kedavra
 */
public class GroupDao {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        List<GroupModel> test = getGroups(6);
        for (GroupModel group : test) {
            System.out.println("Ten Group: " + group.getName());
            for (UserModel user : group.getMember()) {
                System.out.println("--- " + user.getFullname());
            }
        }
    }

    public static List<GroupModel> getGroups(int userId) throws SQLException, ClassNotFoundException {
        List<UserModel> users = new ArrayList<>();
        List<GroupModel> groups = new ArrayList<>();

        UserDao userDao = new UserDao();
        users = userDao.getUsers();

        // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sqlGetGroup = "Select * from room";

        // Thực thi câu lệnh SQL trả về đối tượng ResultSet.
        ResultSet rs = statement.executeQuery(sqlGetGroup);

        // Duyệt trên kết quả trả về.
        while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
            boolean checkJoined = false;
            List<ParticipantModel> participants = new ArrayList<>();
            List<UserModel> members = new ArrayList<>();

            int rsIdRoom = rs.getInt(1);
            String rsName = rs.getString(2);

            // Get list participants
            participants.clear();
            participants.addAll(getParticipants(rsIdRoom));

            // Check join
            for (ParticipantModel tmpParticipant : participants) {
                if (tmpParticipant.getUserId() == userId) {
                    checkJoined = true;
                    break;
                }
            }

            if (checkJoined) {
                System.out.println("LIST PARTI: " + participants.size());
                // Get list members
                for (ParticipantModel participant : participants) {
                    List<UserModel> tempUser = users
                            .stream()
                            .filter(q -> q.getId() == participant.getUserId())
                            .collect(Collectors.toList());

                    members.add(tempUser.get(0));
                }

                // Add group
                groups.add(new GroupModel(rsIdRoom, rsName, members));
            }
        }
        // Đóng kết nối
        connection.close();

        return groups;
    }

    public static List<ParticipantModel> getParticipants(int roomId) throws SQLException, ClassNotFoundException {
        List<ParticipantModel> participants = new ArrayList<>();

        // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sqlGetParticipant = "SELECT * FROM participants WHERE roomid = " + roomId;
        ResultSet rsParticipant = statement.executeQuery(sqlGetParticipant);

        // Duyệt trên kết quả trả về.
        while (rsParticipant.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
            int rsId = rsParticipant.getInt(1);
            int rsUserId = rsParticipant.getInt(2);
            int rsRoomId = rsParticipant.getInt(3);
            Date rsJoinAt = rsParticipant.getDate(4);

            participants.add(new ParticipantModel(rsId, rsUserId, rsRoomId, rsJoinAt));
        }

        // Đóng kết nối
        connection.close();

        return participants;
    }
}
