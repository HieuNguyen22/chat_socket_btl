                    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tic.tac.toe.server.controller;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;
import tic.tac.toe.dao.UserDao;
import tic.tac.toe.dto.MatchInfoDTO;
import models.User;
import utils.SocketUtils;

/**
 *
 * @author 84978
 */
public class GameServerClientController implements Runnable {
    
    public GameServerController gameServerController;
    public int sessionNum;
    
    public Socket client;
    public ObjectOutputStream os;
    public ObjectInputStream is;
    
    public User user;
    
    public Boolean isClosed;
    public Boolean isPlaying;
    
    public int turn;
    
    public GameServerClientController(GameServerController gameServerController, Socket client, int sessionNum) {
        this.gameServerController = gameServerController;
        this.sessionNum = sessionNum;
        this.client = client;
        isClosed = false;
        isPlaying = false;
    }

    
    @Override
    public void run() {
        
        try {
            
            is = new ObjectInputStream(client.getInputStream());
            os = new ObjectOutputStream(client.getOutputStream());
            SocketUtils.writeUTF(os, "Connect to server successfull");
            
            String cmd;
            while (!isClosed) {
                while(isPlaying) {
                    Thread.sleep(100);
                }
                System.out.println("Sesstion: " + sessionNum + " - Waiting command from client...");
                cmd = SocketUtils.readUTF(is);
                System.out.println("Sesstion: " + sessionNum + " - Command: " + cmd);
                if (cmd != null){   
                    gameServerController.logs.append(
                            new Date() + ": Sesstion: " + sessionNum + " - Command from client " + client.getInetAddress().getHostAddress() + " ["+ cmd +"] \n"
                    );
		    handleClientCmd(cmd);   
		}
            }
        } catch (Exception ex) {
            System.out.println("Sesstion: " + sessionNum + " - Command from client is error");
        }
    }
    
    
    private void handleClientCmd(String clientCommand) {
        GameServerClientController partner;
        try {
            String[] items = clientCommand.split(" ", 2);
            String cmd = items[0].toLowerCase();
            String args = "";
            if(items.length > 1) {
                args = items[1];
            }

            switch (cmd) {
                case "login":
                    items = args.split(" ", 2);
                    GameServerClientController existUser = gameServerController.getUserSocketByUserName(items[0]);
                    
                    User userClient = UserDao.login(items[0], items[1]);
                    if(existUser == null && userClient != null) {
                        this.user = userClient;
                        SocketUtils.write(os, userClient);
                        gameServerController.logs.append(
                            new Date() + ": Sesstion: " + sessionNum + " - User Logined: " + userClient + "\n"
                    );
                    } else {
                        SocketUtils.write(os, null);
                    }
                    break;
                case "logout":
                    this.user = null;
                    SocketUtils.writeUTF(os, "logout ");
                    break;
                case "start_game":
                    partner = gameServerController.getUserSocketByUserName(args);
                    turn = 1;
                    if(partner != null) {
                        boolean isPlaying = gameServerController.startGame(this, partner);
                        if(isPlaying) {
                            SocketUtils.writeUTF(os, "notification Player " + args + " is playing!!!");
                        } else {
                            MatchInfoDTO matchInfoDTO = UserDao.getMatchInfo(user.getTenDangNhap(), partner.user.getTenDangNhap());
                                    
                            if(matchInfoDTO.getMyUsername().equals(user.getTenDangNhap())) {
                                SocketUtils.writeUTF(
                                        os, 
                                        "start_game " + partner.user.getTenDangNhap() + " " + matchInfoDTO.getNumWin() + " " + matchInfoDTO.getNumDraw() + " " + matchInfoDTO.getNumLost()
                                );
                                SocketUtils.writeUTF(
                                        partner.os, 
                                        "start_game " + user.getTenDangNhap() + " " + matchInfoDTO.getNumLost() + " " + matchInfoDTO.getNumDraw() + " " + matchInfoDTO.getNumWin()
                                );
                            } else {
                                SocketUtils.writeUTF(
                                        os, 
                                        "start_game " + partner.user.getTenDangNhap() + " " + matchInfoDTO.getNumLost() + " " + matchInfoDTO.getNumDraw() + " " + matchInfoDTO.getNumWin()
                                );
                                SocketUtils.writeUTF(
                                        partner.os, 
                                        "start_game " + user.getTenDangNhap() + " " + matchInfoDTO.getNumWin() + " " + matchInfoDTO.getNumDraw() + " " + matchInfoDTO.getNumLost()
                                );
                            }
                        }
                    } else {
                        SocketUtils.writeUTF(os, "notification Not found client with username " + args );
                    }
                    break;
                case "is_playing":
                    this.isPlaying = Boolean.parseBoolean(args);
                    break;
                case "restart_game":   
                    partner = gameServerController.getUserSocketByUserName(args);
                    if(partner != null) {
                        boolean isPlaying = false;
                        if(turn == 1) {
                            isPlaying = gameServerController.startGame(partner, this);
                        } else {
                            isPlaying = gameServerController.startGame(this, partner);
                        }
                        
                        if(isPlaying) {
                            SocketUtils.writeUTF(os, "notification Player " + args + " is playing!!!");
                        } else {
                            if(turn == 1) {
                                SocketUtils.writeUTF(os, "restart_game " + partner.user.getTenDangNhap());
                                SocketUtils.writeUTF(partner.os, "restart_game " + user.getTenDangNhap());
                            } else {
                                
                                SocketUtils.writeUTF(partner.os, "restart_game " + user.getTenDangNhap());
                                SocketUtils.writeUTF(os, "restart_game " + user.getTenDangNhap());
                            }
                        }
                        
                        turn = 0;
                    } else {
                        SocketUtils.writeUTF(os, "notification Not found client with username " + args );
                    }
                    break;
                case "partner_win":
                    partner = gameServerController.getUserSocketByUserName(args);
                    SocketUtils.writeUTF(partner.os, "my_win " + user.getTenDangNhap());
                    break;
                case "close_game":
                    partner = gameServerController.getUserSocketByUserName(args);
                    SocketUtils.writeUTF(partner.os, "close_game " + user.getTenDangNhap());
                    break;
                case "close_client":
                    this.gameServerController.sockerClients.remove(this);
                    this.gameServerController.playingClients.remove(this);
                    this.isClosed = true;
                    this.client.close();
                    break;
                default:
                    // Unknown command
                    break;
            }
        } catch(Exception exception) {
            exception.printStackTrace();
        }
       
    }
    
    public Socket getClient() {
        return client;
    }

    public void setClient(Socket client) {
        this.client = client;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    
}
