/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tic.tac.toe.dto;

/**
 *
 * @author 84978
 */
public class MatchInfoDTO {
    private String myUsername;
    private String partnerUsername;
    private int numWin;
    private int numLost;
    private int numDraw;
    
    public MatchInfoDTO(){}
    
    public MatchInfoDTO(String myUsername, String partnerUsername, int numWin, int numLost, int numDraw){
        this.myUsername = myUsername;
        this.partnerUsername = partnerUsername;
        this.numWin = numWin;
        this.numLost = numLost;
        this.numDraw = numDraw;
    }

    public String getMyUsername() {
        return myUsername;
    }

    public void setMyUsername(String myUsername) {
        this.myUsername = myUsername;
    }

    public String getPartnerUsername() {
        return partnerUsername;
    }

    public void setPartnerUsername(String partnerUsername) {
        this.partnerUsername = partnerUsername;
    }

    public int getNumWin() {
        return numWin;
    }

    public void setNumWin(int numWin) {
        this.numWin = numWin;
    }

    public int getNumLost() {
        return numLost;
    }

    public void setNumLost(int numLost) {
        this.numLost = numLost;
    }

    public int getNumDraw() {
        return numDraw;
    }

    public void setNumDraw(int numDraw) {
        this.numDraw = numDraw;
    }
    
    
}
