
package controller;

import client.ClientF;
import client.GameClientView;
import java.awt.Frame;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import models.User;
import utils.Constants;
import utils.Logs;
import utils.SocketUtils;
import utils.TicTacToeConstants;

public class GameClientController implements TicTacToeConstants {
    
    public Socket socket;
    public ObjectInputStream fromServer;
    public ObjectOutputStream toServer;
    
    public GameClientView clientView;
    
    public ClientF frame;
    
    public User user;
    
    public String serverHost = Constants.SERVER_HOST;
    public int serverPort = Constants.SERVER_PORT;
    
    public boolean isPlaying = false;
    public boolean isClosed = false;

    public GameClientController() {
    }
    
    public GameClientController(User user) {
        this.user = user;
    }
    
    public void connectToServer() {
        try {
            Logs.log("Request connect to server with host: " + serverHost + ", port: " + serverPort);
            socket = new Socket(serverHost, serverPort);
             
            toServer = new ObjectOutputStream(socket.getOutputStream());
            fromServer = new ObjectInputStream(socket.getInputStream());
            
            String logServer = SocketUtils.readUTF(fromServer);
            Logs.log(new Date() + ": " + logServer);
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void handleGame() {
        new Thread(() -> {
            try {
                String clientCommand;
                while(!isClosed) {
                    while(isPlaying) {
                        Thread.sleep(100);
                    }
                    System.out.println("Waiting command from server...");
                    clientCommand = SocketUtils.readUTF(fromServer);
                    System.out.println("Command: " + clientCommand);
                    if(clientCommand != null) {
                        String[] items = clientCommand.split(" ", 2);
                        String cmd = items[0].toLowerCase();
                        String args = "";
                        if(items.length > 1) {
                            args = items[1];
                        }

                        switch (cmd) {
                            case "start_game":
                                Logs.log("Start game with : " + args);
                                sendToServerIsPlaying(true);
                                clientView = new GameClientView(this.frame, false);
                                clientView.setVisible(true);
                                clientView.setClientController(this);
                                clientView.init();
                                String scores[] = args.split(" ", 4);
                                clientView.setPartner(scores[0]);
                                clientView.setNumWin(scores[1]);
                                clientView.setNumDraw(scores[2]);
                                clientView.setNumLost(scores[3]);
                                isPlaying = true;
                                this.frame.getBtnGame().setEnabled(false);
                                break;
                            case "restart_game":
//                                stopGame();
                                Logs.log("Restart game with : " + args);
                                sendToServerIsPlaying(true);
                                clientView.clear();
                                clientView.init();
                                isPlaying = true;
                                this.frame.getBtnGame().setEnabled(false);
                                break;
                            case "my_win":
                                Logs.log("I win the game with : " + args);
                                this.clientView.handleWinner(PLAYER1);
                                break;
                            case "stop_game":
                                if(args.length() == 0) {
                                    JOptionPane.showMessageDialog(this.clientView, "Your partner exit this game, You win!!");
                                    this.clientView.dispose();
                                    this.clientView = null;
                                }
                                Logs.log("Stop game !!");
                                stopGame(0);
                                this.frame.getBtnGame().setEnabled(true);
                                break;
                            case "close_game":   
                                if (clientView != null) {
                                    JOptionPane.showMessageDialog(this.clientView, "Your partner exit this game!!");
                                    this.clientView.dispose();
                                    this.clientView = null;
                                    this.frame.getBtnGame().setEnabled(true);
                                }
                                break;
                            case "notification":
                                JOptionPane.showMessageDialog(this.frame, args);
                                break;
                            case "logout":
                                this.user = null;
                                this.clientView = null;
                                this.isClosed = true;
                                break;
                        }
                    }
                }
            } catch(Exception e) {
                e.printStackTrace();
            }
        }).start();
    }
    
    
    public User login(String username, String password) {
        try {
            
            Logs.log("Request login to server with username: " + username + " " + password);
            SocketUtils.writeUTF(toServer, "login " + username + " " + password);
            user = (User) SocketUtils.read(fromServer);
            Logs.log("Response form server get user: " + user);
            
            this.isClosed = false;
            handleGame();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return user;
    }
    
    public void logout() {
        try {
            
            Logs.log("Request logout to server with username: " + user.getTenDangNhap());
            SocketUtils.writeUTF(toServer, "logout " + user.getTenDangNhap());
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    public void startGame(String username) {
        try {
            Logs.log("Request start game to server with username: " + username);
            SocketUtils.writeUTF(toServer, "start_game " + username);
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    public void restartGame(String username) {
        try {
            Logs.log("Request restart game to server with username: " + username);
            SocketUtils.writeUTF(toServer, "restart_game " + username);
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    public void partnerWin(String username) {
        try {
            Logs.log("Request partner win of " + user.getTenDangNhap()+ " to server with partner username: " + username);
       
            SocketUtils.writeUTF(toServer, "partner_win " + username);
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    public void stopGame(int player) {
        try {
            Logs.log("Request stop game of " + user.getTenDangNhap());
            
            SocketUtils.writeUTF(toServer, "stop_game " + player);
            this.frame.getBtnGame().setEnabled(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
    }
    
    public void sendToServerIsPlaying(Boolean isPlaying) throws IOException {
        Logs.log("Request send status playing of game: " + isPlaying);
        SocketUtils.writeUTF(toServer, "is_playing " + isPlaying);
    }
    
    public void closeGameWhenNotPlaying(String partner) {
        Logs.log("Request close game");
        try {
            SocketUtils.writeUTF(toServer, "close_game " + partner);
            this.frame.getBtnGame().setEnabled(true);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    public boolean closeClient() {
        this.frame.getBtnGame().setEnabled(true);
        Logs.log("Request close session!!!!!!!!!!!!!!!!!!!!!!!!!!");
        try {
            SocketUtils.writeUTF(toServer, "close_client ");
            
            this.socket.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public void setFrame(ClientF frame) {
        this.frame = frame;
    }
    
    public void setGameView(GameClientView aThis) {
        this.clientView = aThis;
    }
    
    public static void main(String[] args) {
        Random rand = new Random();
                        int randomNum = rand.nextInt(2);
                        System.out.println(randomNum);
    }

    

    

    

    
}
