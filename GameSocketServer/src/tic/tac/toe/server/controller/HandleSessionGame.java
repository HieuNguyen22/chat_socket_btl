/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tic.tac.toe.server.controller;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Date;
import tic.tac.toe.dao.UserDao;
import utils.SocketUtils;
import utils.TicTacToeConstants;
import static utils.TicTacToeConstants.CONTINUE;
import static utils.TicTacToeConstants.DRAW;
import static utils.TicTacToeConstants.PLAYER1_WON;
import static utils.TicTacToeConstants.PLAYER2_WON;


public class HandleSessionGame implements Runnable, TicTacToeConstants {

    private GameServerClientController client1;
    private GameServerClientController client2;
    private Socket player1; 
    private Socket player2;

    private char[][] cell = new char[3][3];

    private DataInputStream fromPlayer1;
    private DataOutputStream toPlayer1;
    private DataInputStream fromPlayer2;
    private DataOutputStream toPlayer2;

    private boolean continueToPlay = true;

 
    public HandleSessionGame(GameServerClientController client1, GameServerClientController client2) {
        this.client1 = client1;
        this.client2 = client2;
        this.player1 = client1.getClient();
        this.player2 = client2.getClient();

        // Khởi tạo ô chơi trống
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cell[i][j] = ' ';
            }
        }
    }

    /**
     * Implement the run() method for the thread
     */
    public void run() {
        try {
            while(!this.client1.isPlaying || !this.client2.isPlaying) {
                Thread.sleep(100); 
            }
            
            fromPlayer1 = new DataInputStream(
                    player1.getInputStream());
            toPlayer1 = new DataOutputStream(
                    player1.getOutputStream());
            fromPlayer2 = new DataInputStream(
                    player2.getInputStream());
            toPlayer2 = new DataOutputStream(
                    player2.getOutputStream());

            SocketUtils.writeInt(toPlayer1, 1);
            SocketUtils.writeInt(toPlayer2, 2);
            
            new Thread(() -> {
                try {
                    while(true) {
                        System.out.println("Sesstion: " + client1.sessionNum + " - Waiting command game from client...");
                        String cmd = SocketUtils.readUTF(client1.is);
                        String items[] = cmd.split(" ", 2);
                        System.out.println("Sesstion: " + client1.sessionNum + " - Command Game: " + cmd);
                        String status = items[0];
                        String args = items[1];
                        
                        if(status.equals("playing")) {
                            // Nhận ô của player 1 đã đánh
                            int row = Integer.parseInt(args.split(" ")[0]);
                            int column = Integer.parseInt(args.split(" ")[1]);
                            System.out.println("player1 " + row + " " + column);
                            cell[row][column] = 'X'; // Thiết lập là x

                            // kiểm tra player 1 có win không
                            if (isWon('X')) {
                                saveClientOneWin();
                                SocketUtils.writeInt(toPlayer1, PLAYER1_WON);
                                SocketUtils.writeInt(toPlayer2, PLAYER1_WON);
                                sendMove(toPlayer2, row, column);
                                
                                SocketUtils.writeUTF(client2.os, "stop_game -1");
                                break;
                            } else if (isFull()) { // KIểm tra xem có hòa không
                                saveDraw();
                                SocketUtils.writeInt(toPlayer1, DRAW);
                                SocketUtils.writeInt(toPlayer2, DRAW);
                                sendMove(toPlayer2, row, column);
                                
                                SocketUtils.writeUTF(client2.os, "stop_game -1");
                                break;
                            } else {
                                // Thông báo đã đến lượt player 2;
                                SocketUtils.writeInt(toPlayer2, CONTINUE);

                                // Gửi ô mà player1 đã chọn đến player 2
                                sendMove(toPlayer2, row, column);
                            }
                        } else if (status.equals("stop_game")){
                            if(args.equals("1")) {
                                saveClientTwoWin();
                                SocketUtils.writeInt(toPlayer1, -1);
                                SocketUtils.writeInt(toPlayer2, -1);
                                
                                SocketUtils.writeUTF(client2.os, "stop_game ");
                            } else if(args.equals("2")) {
                                saveClientTwoWin();
                                SocketUtils.writeInt(toPlayer1, -1);
                                SocketUtils.writeInt(toPlayer2, -1);
                                
                                SocketUtils.writeUTF(client2.os, "stop_game ");
                            }
                            
                            break;
                        }
                    } 
                    
                    stopGameClient1();
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }).start();
            
            new Thread(() -> {
                try {
                    while(true) {
                        System.out.println("Sesstion: " + client2.sessionNum + " - Waiting command game from client...");
                        String cmd = SocketUtils.readUTF(client2.is);
                        String items[] = cmd.split(" ", 2);
                        System.out.println("Sesstion: " + client2.sessionNum + " - Command Game: " + cmd);
                        String status = items[0];
                        String args = items[1];
                        
                        if(status.equals("playing")) {
                            // Nhận ô của player 2 đã đánh
                            int row = Integer.parseInt(args.split(" ")[0]);
                            int column = Integer.parseInt(args.split(" ")[1]);
                            cell[row][column] = 'O';

                            // kiểm tra player 2 có win không
                            if (isWon('O')) {
                                saveClientTwoWin();
                                SocketUtils.writeInt(toPlayer1, PLAYER2_WON);
                                SocketUtils.writeInt(toPlayer2, PLAYER2_WON);
                                sendMove(toPlayer1, row, column);
                                
                                SocketUtils.writeUTF(client1.os, "stop_game -1");
                                break;
                            } else {
                                // Thông báo đã đến lượt player 1
                                SocketUtils.writeInt(toPlayer1, CONTINUE);

                                // Gửi ô mà player2 đã chọn đến player 1
                                sendMove(toPlayer1, row, column);
                            }
                        } else if (status.equals("stop_game")){
                            if(args.equals("1")) {
                                saveClientOneWin();
                                SocketUtils.writeInt(toPlayer1, -1);
                                SocketUtils.writeInt(toPlayer2, -1);
                                
                                SocketUtils.writeUTF(client1.os, "stop_game ");
                            } else if(args.equals("2")) {
                                saveClientOneWin();
                                SocketUtils.writeInt(toPlayer1, -1);
                                SocketUtils.writeInt(toPlayer2, -1);
                                
                                SocketUtils.writeUTF(client1.os, "stop_game ");
                            }
                            
                            break;
                        }
                    } 
                    
                    stopGameClient2();
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }).start();

            
        } catch (Exception ex) {
            ex.printStackTrace();
            
        } 
        
//        stopGame();
    }
    
    private void saveClientOneWin() {
        UserDao.incrementWin(client1.getUser().getTenDangNhap(), client2.getUser().getTenDangNhap());
    }
    
    private void saveClientTwoWin() {
        UserDao.incrementWin(client2.getUser().getTenDangNhap(), client1.getUser().getTenDangNhap());
    }
    
    private void saveDraw() {
        UserDao.incrementDraw(client1.getUser().getTenDangNhap(), client2.getUser().getTenDangNhap());
    }
    
    private void stopGameClient1() {
        
        String log = new Date() + 
                ": Stop a game for client1: " + 
                client1.getClient().getInetAddress().getHostAddress() + "/" + client1.getUser().getTenDangNhap();
        
        client1.gameServerController.logs.append(log + "\n");
        client1.isPlaying = false;
        client1.gameServerController.unPlaying(client1.getUser().getTenDangNhap());
    }
    
    private void stopGameClient2() {
        
        String log = new Date() + 
                ": Stop a game for client2: " + 
                client2.getClient().getInetAddress().getHostAddress() + "/" + client2.getUser().getTenDangNhap();
        
        client2.gameServerController.logs.append(log + "\n");
        client2.isPlaying = false;
        client2.gameServerController.unPlaying(client2.getUser().getTenDangNhap());
    }

    private void sendMove(DataOutputStream out, int row, int column)
            throws IOException {
        SocketUtils.writeInt(out, row);
        SocketUtils.writeInt(out, column);
    }

    private boolean isFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (cell[i][j] == ' ') {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isWon(char token) {
        for (int i = 0; i < 3; i++) {
            if ((cell[i][0] == token)
                    && (cell[i][1] == token)
                    && (cell[i][2] == token)) {
                return true;
            }
        }

        for (int j = 0; j < 3; j++) {
            if ((cell[0][j] == token)
                    && (cell[1][j] == token)
                    && (cell[2][j] == token)) {
                return true;
            }
        }

        if ((cell[0][0] == token)
                && (cell[1][1] == token)
                && (cell[2][2] == token)) {
            return true;
        }

        if ((cell[0][2] == token)
                && (cell[1][1] == token)
                && (cell[2][0] == token)) {
            return true;
        }

        return false;
    }
}
