/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package client;

import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.List;
import static java.util.function.Predicate.not;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import models.Data;
import models.Messages;
import models.UserModel;
import models.Users;
//import model.UserLogin;
//import view.MainFrame;

/**
 *
 * @author Admin
 */
public class Login extends javax.swing.JFrame {

    /**
     * Creates new form Login
     */
    BufferedWriter os;
    BufferedReader is;
    private Thread thread;
    private int myId;
    private int clientId;
    SignUp signup;
    ClientF client;
    Socket socket;
    public boolean attachmentOpen = false;
    private boolean isConnected = false;
    SignUp signUp;

    public int getMyId() {
        return myId;
    }

    public void setID(int myId) {
        this.myId = myId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    private void setIDTitle() {
        this.setTitle("Client " + this.myId);
    }

    public Login() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        setupSocket();

    }

    void setupSocket() {
        try {
            thread = new Thread() {
                @Override
                public void run() {

                    try {
                        // Gửi yêu cầu kết nối tới Server đang lắng nghe
                        // trên máy 'localhost' cổng 7777.
                        socket = new Socket("localhost", 7777);
                        System.out.println("Kết nối thành công!");
                        // Tạo luồng đầu ra tại client (Gửi dữ liệu tới server)
                        os = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                        // Luồng đầu vào tại Client (Nhận dữ liệu từ server).
                        is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                        String message;
                        while (true) {
                            message = is.readLine();
                            System.out.println(message);
                            if (message == null) {
                                System.out.println("OK");

                            }
                            String[] messageSplit = message.split(",");
                            if (messageSplit[0].equals("get-id")) {
                                setID(Integer.parseInt(messageSplit[1]));
                                setIDTitle();
                            }
                            if (messageSplit[0].equals("login ok")) {
                                setVisible(false);
                                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                                System.out.println("da tao luong nhan");
                                Users allfriends = (Users) ois.readObject();
                                System.out.println(allfriends.getUsers().size());
                                System.out.println("da nhan duoc object");
                                client = new ClientF();
                                client.setSocket(socket, os, is);
                                client.setID(myId);
                                client.users = allfriends.getUsers();
//                                System.out.println("nhan object okie");

                                client.setDefaultValues();
                                client.catchEventOnline();
                                client.setVisible(true);

                            }
                            if (messageSplit[0].equals("login failed")) {
//                                 JOptionPane.showMessageDialog(this, "Login failed","Thông báo",JOptionPane.OK_OPTION);
                            }
                            if (messageSplit[0].equals("sign up failed")) {

                                System.out.println("dang ky loi");
                            }

                            if (messageSplit[0].equals("allfriends")) {
                                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                                List<UserModel> allfriends = (List<UserModel>) ois.readObject();
                                client.users = allfriends;
                                System.out.println("nhan object okie");
                            }
                            if (messageSplit[0].equals("message")) {
                                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                                Messages k = (Messages) ois.readObject();
                                client.messages = k.getListmessages();
                                client.refreshMessages();
                            }
                            if (messageSplit[0].equals("person-message")) {

                                JLabel text = new JLabel();
                                text.setText(messageSplit[1]);

                                JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                                tmpPanel.add(text);

                                Border border = tmpPanel.getBorder();
                                Border margin = new EmptyBorder(10, 10, 10, 10);
                                tmpPanel.setBorder(new CompoundBorder(border, margin));

                                client.content.add(tmpPanel);
                                client.content.setLayout(new BoxLayout(client.content, BoxLayout.Y_AXIS));

                                client.jMessageScrollPanelsetViewportView(client.content);
                            }

                            if (messageSplit[0].equals("person-icon")) {
                                client.createIconMessage(Integer.parseInt(messageSplit[1]), Integer.parseInt(messageSplit[2]));
                            }

                            if (messageSplit[0].equals("get-file")) {
                                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                                Data data = (Data) ois.readObject();
                                File myObj = new File("D:/files_btl_ltm/file_client/" + data.getName());

                                myObj.createNewFile();
                                OutputStream out = new FileOutputStream("D:/files_btl_ltm/file_client/" + data.getName());
                                out.write(data.getFile());
                                out.close();
                            }
                            if (messageSplit[0].equals("person-file")) {

                                JButton text = new JButton();
                                text.setText(messageSplit[1]);

                                JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                                tmpPanel.add(text);

                                Border border = tmpPanel.getBorder();
                                Border margin = new EmptyBorder(10, 10, 10, 10);
                                tmpPanel.setBorder(new CompoundBorder(border, margin));

                                client.content.add(tmpPanel);
                                client.content.setLayout(new BoxLayout(client.content, BoxLayout.Y_AXIS));

                                client.jMessageScrollPanelsetViewportView(client.content);

                                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                                Data data = (Data) ois.readObject();
                                File myObj = new File("D:/files_btl_ltm/file_client/" + data.getName());

                                myObj.createNewFile();
                                OutputStream out = new FileOutputStream("D:/files_btl_ltm/file_client/" + data.getName());
                                out.write(data.getFile());
                                out.close();

                                text.addActionListener((ActionListener) new java.awt.event.ActionListener() {
                                    @Override
                                    public void actionPerformed(ActionEvent e) {
                                        Desktop d = null;
                                        if (Desktop.isDesktopSupported()) {
                                            System.out.println("Co sp");
                                            File f = new File("D:/files_btl_ltm/file_client/" + data.getName());
                                            if (!f.exists()) {
                                                try {
                                                    System.out.println("Getfile demo");
                                                    os.write("Get File," + data.getName());
                                                    os.newLine();
                                                    os.flush();
                                                } catch (IOException ex) {
                                                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                                                }

                                            }
                                            try {
                                                Desktop.getDesktop().open(f);
                                            } catch (IOException ex) {
                                                Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                                            }
                                        }

                                    }

                                });
                            }

                            if (messageSplit[0].equals("refresh-messages")) {
                                client.content.removeAll();
                                client.refreshMessages();
                            }

//                            if (messageSplit[0].equals("update-online-list")) {
//                                onlineList = new ArrayList<>();
//                                String online = "";
//                                String[] onlineSplit = messageSplit[1].split("-");
//                                for (int i = 0; i < onlineSplit.length; i++) {
//                                    onlineList.add(onlineSplit[i]);
//                                    online += "Client " + onlineSplit[i] + " đang online\n";
//                                }
//                                taOnline.setText(online);
//                                //updateCombobox();
//                            }
//                            if (messageSplit[0].equals("global-message")) {
//                                taMessage.setText(taMessage.getText() + messageSplit[1] + "\n");
//                                taMessage.setCaretPosition(taMessage.getDocument().getLength());
//                            }
                        }
//                    os.close();
//                    is.close();
//                    socketOfClient.close();
                    } catch (UnknownHostException e) {
                        return;
                    } catch (IOException e) {
                        return;
                    } catch (SQLException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (ClassNotFoundException ex) {
                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };
            thread.run();
        } catch (Exception e) {
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        username_tf = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        pass_tf = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Tên đăng nhập");

        jLabel2.setText("Mật khẩu");

        jButton1.setText("Đăng nhập");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Đăng kí");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(100, 100, 100)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(username_tf, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                            .addComponent(pass_tf))))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(username_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(pass_tf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(49, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
public void ShowNot(String not) {
        JOptionPane.showMessageDialog(this, not, "Thông báo", JOptionPane.OK_OPTION);
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        signup = new SignUp();
        signup.socket = this.socket;
        signup.os = this.os;
        signup.is = this.is;
        signup.setVisible(true);

    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        String username = username_tf.getText();
        String password = pass_tf.getText();
        try {
            os.write("LOGIN" + "," + username + "," + password);
            os.newLine();
            os.flush();
        } catch (IOException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Login.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        Login login = new Login();
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new Login().setVisible(true);
//            }
//        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField pass_tf;
    private javax.swing.JTextField username_tf;
    // End of variables declaration//GEN-END:variables
}
