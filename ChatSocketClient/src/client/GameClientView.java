/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package client;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import controller.GameClientController;
import utils.Logs;
import utils.SocketUtils;
import static utils.TicTacToeConstants.DRAW;
import static utils.TicTacToeConstants.PLAYER1;
import static utils.TicTacToeConstants.PLAYER1_WON;
import static utils.TicTacToeConstants.PLAYER2;
import static utils.TicTacToeConstants.PLAYER2_WON;

/**
 *
 * @author 84978
 */
public class GameClientView extends javax.swing.JDialog {
    
    private GameClientController clientController;
    
    private DataInputStream fromServer;
    private DataOutputStream toServer;
    
    private int rowSelected;
    private int columnSelected;
    
    private boolean myTurn = false;

    private char myToken = ' ';

    private char otherToken = ' ';
    
            
    private boolean continueToPlay = true;
    
    public String usernamePartner;
    
    private long startTime;
    
    private Thread timerThread;
    
    private int player;

    /**
     * Creates new form GameClientView
     */
    public GameClientView(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        
    }
    
    public void setClientController(GameClientController clientController) {
        this.clientController = clientController;
        this.clientController.setGameView(this);
    }
    
    public void setPartner(String username) {
        this.usernamePartner = username;
        this.user2.setText(username);
    }
    
    private void countTime() {
        
        startTime = System.currentTimeMillis();
        timerThread = new Thread(() -> {
            while(true) {
                long current = System.currentTimeMillis();
                long diff = (current - startTime) / 1000;

                long seconds = 0, minutes = 0, hours = 0;

                if(diff >= 60) {
                    seconds = diff - ((int) diff / 60) * 60;
                    diff = diff / 60;
                    if(diff >= 60) {
                        minutes = diff - ((int) diff / 60) * 60;
                        hours = diff / 60;
                    } else {
                        minutes = diff;
                    }
                } else {
                    seconds = diff;
                }

                String time = "%02d:%02d:%02d".formatted(hours, minutes, seconds);
                timer.setText(time);
                try {
                    Thread.sleep(1000L);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });
        timerThread.start();
    }
    
    public void clear() {
        btn00.setText("");
        btn01.setText("");
        btn02.setText("");
        btn10.setText("");
        btn11.setText("");
        btn12.setText("");
        btn20.setText("");
        btn21.setText("");
        btn22.setText("");
        
        btn00.setEnabled(true);
        btn01.setEnabled(true);
        btn02.setEnabled(true);
        btn10.setEnabled(true);
        btn11.setEnabled(true);
        btn12.setEnabled(true);
        btn20.setEnabled(true);
        btn21.setEnabled(true);
        btn22.setEnabled(true);
        
        logs.setText("");
        myTurn = false;
        myToken = ' ';
        otherToken = ' ';
        continueToPlay = true;
    }
    
    public void clearButtonTurn() {
        tokenUser1.setOpaque(false);
        tokenUser2.setOpaque(false);
        tokenUser1.setBackground(Color.WHITE);
        tokenUser2.setBackground(Color.WHITE);
    }
    
    public void init() {
        
        countTime();
        newGame.setEnabled(false);
        new Thread(() -> {
            try {
                toServer = new DataOutputStream(this.clientController.socket.getOutputStream());
                fromServer = new DataInputStream(this.clientController.socket.getInputStream());
                player = fromServer.readInt();

                // Am I player 1 or 2?
                if (player == PLAYER1) {
                    myToken = 'X';
                    otherToken = 'O';
                    tokenUser1.setText(myToken + "");
                    tokenUser2.setText(otherToken + "");
                   
                    
                    // It is my turn
                    myTurn = true;
                    tokenUser1.setOpaque(true);
                    tokenUser2.setOpaque(false);
                    tokenUser1.setBackground(Color.GREEN);
                    tokenUser2.setBackground(Color.WHITE);
                
                } else if (player == PLAYER2) {
                    myToken = 'O';
                    otherToken = 'X';
                    tokenUser1.setText(myToken + "");
                    tokenUser2.setText(otherToken + "");
                    
                    
                    tokenUser1.setOpaque(false);
                    tokenUser2.setOpaque(true);
                    tokenUser1.setBackground(Color.WHITE);
                    tokenUser2.setBackground(Color.GREEN);
                }

                // Continue to play
                while (continueToPlay) {
                    if (player == PLAYER1) {
//                        waitForPlayerAction(); // Wait for player 1 to move
//                        if(!continueToPlay) break;
//                        sendMove(); // Send the move to the server
                        receiveInfoFromServer(); // Receive info from the server
                        if(!continueToPlay) break;
                    } else if (player == PLAYER2) {
                        receiveInfoFromServer(); // Receive info from the server
                        if(!continueToPlay) break;
//                        waitForPlayerAction(); // Wait for player 2 to move
//                        if(!continueToPlay) break;
//                        sendMove(); // Send player 2's move to the server
                    }
                }
                
            } catch (Exception ex) {
                
                ex.printStackTrace();
            }
            
            stopPlaying();
        }).start();
    }
    
    public void stopPlaying(){
        Logs.log("Stop playing");
        newGame.setEnabled(true);
        clientController.isPlaying = false;
        timerThread.stop();
        continueToPlay = false;
        
        btn00.setEnabled(false);
        btn01.setEnabled(false);
        btn02.setEnabled(false);
        btn10.setEnabled(false);
        btn11.setEnabled(false);
        btn12.setEnabled(false);
        btn20.setEnabled(false);
        btn21.setEnabled(false);
        btn22.setEnabled(false);
    }
    

    /**
     * Send this player's move to the server
     */
    private void sendMove() throws IOException {
        Logs.log("Send move to server row: " + rowSelected + ", col: " + columnSelected);
//        SocketUtils.writeInt(toServer, rowSelected);
//        SocketUtils.writeInt(toServer, columnSelected);
        SocketUtils.writeUTF(clientController.toServer, "playing " + rowSelected + " " + columnSelected);
    }

    /**
     * Receive info from the server
     */
    private void receiveInfoFromServer() throws IOException {
        // Receive game status
        Logs.log("Receive info form server");
        int status = SocketUtils.readInt(fromServer);

        if(status == -1) {
            continueToPlay = false; 
            return;
        }
        
        if (status == PLAYER1_WON) {
            // Player 1 won, stop playing
            continueToPlay = false;
            if (myToken == 'X') {
                logs.setText("You won! (X)");
                handleWinner(PLAYER1_WON);
            } else if (myToken == 'O') {
                logs.setText(usernamePartner + " (X) has won!");
                handleWinner(PLAYER2_WON);
                receiveMove();
            }
            clearButtonTurn();
        } else if (status == PLAYER2_WON) {
            // Player 2 won, stop playing
            continueToPlay = false;
            if (myToken == 'O') {
                logs.setText("I won! (O)");
                handleWinner(PLAYER1_WON);
            } else if (myToken == 'X') {
                logs.setText(usernamePartner + " (O) has won!");
                handleWinner(PLAYER2_WON);
                receiveMove();
            }
            clearButtonTurn();
        } else if (status == DRAW) {
            // No winner, game is over
            continueToPlay = false;
            logs.setText("Game is over, no winner!");
            handleDraw();

            if (myToken == 'O') {
                receiveMove();
            }
        } else {
            receiveMove();
            
            myTurn = true; // It is my turn
            tokenUser1.setOpaque(true);
            tokenUser2.setOpaque(false);
            tokenUser1.setBackground(Color.GREEN);
            tokenUser2.setBackground(Color.WHITE);
        }
    }

    private void receiveMove() throws IOException {
        // Get the other player's move
        int row = SocketUtils.readInt(fromServer);
        int column = SocketUtils.readInt(fromServer);
        Logs.log("Receive move row: " + row + ", col: " + column);
        if(row == 0 && column == 0) {
            btn00.setText(otherToken + "");
            btn00.setEnabled(false);
        } else if(row == 0 && column == 1) {
            btn01.setText(otherToken + "");
            btn01.setEnabled(false);
        } else if(row == 0 && column == 2) {
            btn02.setText(otherToken + "");
            btn02.setEnabled(false);
        } else if(row == 1 && column == 0) {
            btn10.setText(otherToken + "");
            btn10.setEnabled(false);
        } else if(row == 1 && column == 1) {
            btn11.setText(otherToken + "");
            btn11.setEnabled(false);
        } else if(row == 1 && column == 2) {
            btn12.setText(otherToken + "");
            btn12.setEnabled(false);
        } else if(row == 2 && column == 0) {
            btn20.setText(otherToken + "");
            btn20.setEnabled(false);
        } else if(row == 2 && column == 1) {
            btn21.setText(otherToken + "");
            btn21.setEnabled(false);
        } else if(row == 2 && column == 2) {
            btn22.setText(otherToken + "");
            btn22.setEnabled(false);
        } 
    }
    
    public void handleWinner(int player) {
        if(player == 1) {
            winUser1.setText(Integer.parseInt(winUser1.getText()) + 1 + "");
        } else {
            winUser2.setText(Integer.parseInt(winUser2.getText()) + 1 + "");
        }
    }
    
     public void handleDraw() {
        draw.setText(Integer.parseInt(draw.getText()) + 1 + "");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btn01 = new javax.swing.JButton();
        btn02 = new javax.swing.JButton();
        btn10 = new javax.swing.JButton();
        btn00 = new javax.swing.JButton();
        btn12 = new javax.swing.JButton();
        btn20 = new javax.swing.JButton();
        btn22 = new javax.swing.JButton();
        btn21 = new javax.swing.JButton();
        btn11 = new javax.swing.JButton();
        user1 = new javax.swing.JLabel();
        user2 = new javax.swing.JLabel();
        winUser2 = new javax.swing.JLabel();
        winUser1 = new javax.swing.JLabel();
        draw = new javax.swing.JLabel();
        time = new javax.swing.JLabel();
        timer = new javax.swing.JTextField();
        closeBtn = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        logs = new javax.swing.JTextField();
        tokenUser1 = new javax.swing.JButton();
        tokenUser2 = new javax.swing.JButton();
        newGame = new javax.swing.JButton();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        btn01.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn01.setAlignmentY(0.0F);
        btn01.setMargin(new java.awt.Insets(14, 14, 14, 14));
        btn01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn01ActionPerformed(evt);
            }
        });

        btn02.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn02ActionPerformed(evt);
            }
        });

        btn10.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn10ActionPerformed(evt);
            }
        });

        btn00.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn00.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn00ActionPerformed(evt);
            }
        });

        btn12.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn12ActionPerformed(evt);
            }
        });

        btn20.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn20ActionPerformed(evt);
            }
        });

        btn22.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn22ActionPerformed(evt);
            }
        });

        btn21.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn21ActionPerformed(evt);
            }
        });

        btn11.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        btn11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn11ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn10, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn11, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn12, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btn20, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn21, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn22, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btn00, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn01, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn02, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn01, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn02, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn00, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn10, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn12, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn11, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn20, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn21, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn22, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        user1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        user1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        user1.setText("Me");

        user2.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        user2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        user2.setText("TrangVT");

        winUser2.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        winUser2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        winUser2.setText("0");

        winUser1.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        winUser1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        winUser1.setText("0");

        draw.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        draw.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        draw.setText("0");

        time.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        time.setText("Timer:");

        timer.setText("00:00:30");
        timer.setEnabled(false);

        closeBtn.setText("Close");
        closeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeBtnActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel2.setText("Logs:");

        logs.setEnabled(false);

        tokenUser1.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        tokenUser1.setText("o");
        tokenUser1.setEnabled(false);
        tokenUser1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tokenUser1ActionPerformed(evt);
            }
        });

        tokenUser2.setFont(new java.awt.Font("Segoe UI", 1, 48)); // NOI18N
        tokenUser2.setText("x");
        tokenUser2.setEnabled(false);
        tokenUser2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tokenUser2ActionPerformed(evt);
            }
        });

        newGame.setText("New Game");
        newGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newGameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(newGame)
                                    .addGap(55, 55, 55)
                                    .addComponent(closeBtn))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(user2, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(33, 33, 33)
                                    .addComponent(user1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(logs, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(19, 19, 19))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(winUser2, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(draw, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(40, 40, 40))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tokenUser2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(winUser1, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tokenUser1))
                        .addGap(57, 57, 57))))
            .addGroup(layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(time)
                .addGap(18, 18, 18)
                .addComponent(timer, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(user1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(user2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(winUser2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(winUser1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(draw, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(tokenUser1)
                            .addComponent(tokenUser2))
                        .addGap(31, 31, 31)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(logs, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(time)
                    .addComponent(timer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(closeBtn)
                    .addComponent(newGame))
                .addContainerGap(43, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn00ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn00ActionPerformed
        // TODO add your handling code here:
        rowSelected = 0;
        columnSelected = 0;
        
        if(myTurn && (btn00.getText() == null || btn00.getText().length() == 0)) {
            btn00.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn00.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn00ActionPerformed

    private void btn01ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn01ActionPerformed
        // TODO add your handling code here:
        rowSelected = 0;
        columnSelected = 1;
         
        if(myTurn && (btn01.getText() == null || btn01.getText().length() == 0)) {
            btn01.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn01.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn01ActionPerformed

    private void btn02ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn02ActionPerformed
        // TODO add your handling code here:
        rowSelected = 0;
        columnSelected = 2;
        
        if(myTurn && (btn02.getText() == null || btn02.getText().length() == 0)) {
            btn02.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn02.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn02ActionPerformed

    private void btn10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn10ActionPerformed
        // TODO add your handling code here:
        rowSelected = 1;
        columnSelected = 0;
        
        if(myTurn && (btn10.getText() == null || btn10.getText().length() == 0)) {
            btn10.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn10.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn10ActionPerformed

    private void btn11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn11ActionPerformed
        // TODO add your handling code here:
        rowSelected = 1;
        columnSelected = 1;
        
        if(myTurn && (btn11.getText() == null || btn11.getText().length() == 0)) {
            btn11.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn11.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn11ActionPerformed

    private void btn12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn12ActionPerformed
        // TODO add your handling code here:
        rowSelected = 1;
        columnSelected = 2;
        
        if(myTurn && (btn12.getText() == null || btn12.getText().length() == 0)) {
            btn12.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn12.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn12ActionPerformed

    private void btn20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn20ActionPerformed
        // TODO add your handling code here:
        rowSelected = 2;
        columnSelected = 0;
        
        if(myTurn && (btn20.getText() == null || btn20.getText().length() == 0)) {
            btn20.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn20.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn20ActionPerformed

    private void btn21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn21ActionPerformed
        // TODO add your handling code here:
        rowSelected = 2;
        columnSelected = 1;
        
        if(myTurn && (btn21.getText() == null || btn21.getText().length() == 0)) {
            btn21.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn21.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn21ActionPerformed

    private void btn22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn22ActionPerformed
        // TODO add your handling code here:
        rowSelected = 2;
        columnSelected = 2;
        
        if(myTurn && (btn22.getText() == null || btn22.getText().length() == 0)) {
            btn22.setText(myToken + "");
            myTurn = false; 
            tokenUser1.setOpaque(false);
            tokenUser2.setOpaque(true);
            tokenUser1.setBackground(Color.WHITE);
            tokenUser2.setBackground(Color.GREEN);
            btn22.setEnabled(false);
            try {
                sendMove();
            } catch (IOException ex) {
                Logger.getLogger(GameClientView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Don't your turn");
        }
    }//GEN-LAST:event_btn22ActionPerformed

    private void tokenUser1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tokenUser1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tokenUser1ActionPerformed

    private void tokenUser2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tokenUser2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tokenUser2ActionPerformed

    private void newGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newGameActionPerformed
        // TODO add your handling code here:
        if(clientController.isPlaying) {
            JOptionPane.showMessageDialog(this, "You is playing with another person!!");
        } else {
            
            clientController.restartGame(usernamePartner);
        }
    }//GEN-LAST:event_newGameActionPerformed

    private void closeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeBtnActionPerformed
        
        // TODO add your handling code here:
        if(clientController.isPlaying) {
           int choose = JOptionPane.showConfirmDialog(this, "You will lost if you leave this game", "Exit this game", JOptionPane.YES_NO_OPTION);
            if(choose == JOptionPane.YES_OPTION) {

                stopPlaying();
                this.clientController.stopGame(player);
                this.clientController.clientView = null;
                this.dispose();
            } 
        } else {
            this.clientController.closeGameWhenNotPlaying(usernamePartner);
            this.dispose();
            this.clientController.clientView = null;
        }
        
    }//GEN-LAST:event_closeBtnActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        // TODO add your handling code here:
        if(clientController.isPlaying) {
                stopPlaying();
                this.clientController.stopGame(player);
                this.clientController.clientView = null;
        } else {
            this.clientController.closeGameWhenNotPlaying(usernamePartner);
            this.clientController.clientView = null;
        }
    }//GEN-LAST:event_formWindowClosed

    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GameClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GameClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GameClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GameClientView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                GameClientView dialog = new GameClientView(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    
    public void setNumWin(String num) {
        this.winUser1.setText(num);
    }
    
    public void setNumDraw(String num) {
        this.draw.setText(num);
    }
    
    public void setNumLost(String num) {
        this.winUser2.setText(num);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn00;
    private javax.swing.JButton btn01;
    private javax.swing.JButton btn02;
    private javax.swing.JButton btn10;
    private javax.swing.JButton btn11;
    private javax.swing.JButton btn12;
    private javax.swing.JButton btn20;
    private javax.swing.JButton btn21;
    private javax.swing.JButton btn22;
    private javax.swing.JButton closeBtn;
    private javax.swing.JLabel draw;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField logs;
    private javax.swing.JButton newGame;
    private javax.swing.JLabel time;
    private javax.swing.JTextField timer;
    private javax.swing.JButton tokenUser1;
    private javax.swing.JButton tokenUser2;
    private javax.swing.JLabel user1;
    private javax.swing.JLabel user2;
    private javax.swing.JLabel winUser1;
    private javax.swing.JLabel winUser2;
    // End of variables declaration//GEN-END:variables
}
