    /*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.io.Serializable;

/**
 *
 * @author Admin
 */
public class User implements Serializable{
   private String hoVaTen;
   private String tenDangNhap;
   private String soDT;
   private String matKhau;

    public User(String hoVaTen, String tenDangNhap, String soDT, String matKhau) {
        this.hoVaTen = hoVaTen;
        this.tenDangNhap = tenDangNhap;
        this.soDT = soDT;
        this.matKhau = matKhau;
    }
    public User(){
}

    public String getHoVaTen() {
        return hoVaTen;
    }

    public String getTenDangNhap() {
        return tenDangNhap;
    }

    public String getSoDT() {
        return soDT;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setHoVaTen(String hoVaTen) {
        this.hoVaTen = hoVaTen;
    }

    public void setTenDangNhap(String tenDangNhap) {
        this.tenDangNhap = tenDangNhap;
    }

    public void setSoDT(String soDT) {
        this.soDT = soDT;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    
    
}
