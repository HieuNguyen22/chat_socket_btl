/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.Data;

/**
 *
 * @author Admin
 */
public class ServerThreadBus {

    private List<ServerThread> listServerThreads;

    public List<ServerThread> getListServerThreads() {
        return listServerThreads;
    }

    public ServerThreadBus() {
        listServerThreads = new ArrayList<>();
    }

    public void add(ServerThread serverThread) {
        listServerThreads.add(serverThread);
    }

    public void mutilCastSend(String message) { //like sockets.emit in socket.io
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            try {
                serverThread.write(message);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void boardCast(int id, String message) {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == id) {
                continue;
            } else {
                try {
                    serverThread.write(message);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public int getLength() {
        return listServerThreads.size();
    }

    public void sendOnlineList() {
        String res = "";
        List<ServerThread> threadbus = Server.serverThreadBus.getListServerThreads();
        for (ServerThread serverThread : threadbus) {
            res += serverThread.getClientNumber() + "-";
        }
        Server.serverThreadBus.mutilCastSend("update-online-list" + "," + res);
    }

    public void sendNotiNewGroup(int id) {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == id) {
                try {
                    serverThread.write("new-group-ok");;
                    break;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void sendMessageToPersion(int id, String message) {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == id) {
                try {
                    serverThread.write("person-message" + "," + message);
                    break;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void sendIconToPersion(int id, String idIcon, String idSender) {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == id) {
                try {
                    serverThread.write("person-icon" + "," + idIcon + "," + idSender);
                    break;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

//    public void sendFileToPersion(int id, String message, Data data) {
//        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
//            if (serverThread.getClientNumber() == id) {
//                try {
//                    serverThread.write("person-file" + "," + message);
//                    ObjectOutputStream os1 = new ObjectOutputStream(serverThread.getSocket().getOutputStream());
//                    os1.writeObject(data);
//                    os1.flush();
//                    System.out.println("Server gui file thanh cong");
//                    break;
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
//            }
//        }
//    }
    public void sendFileToPersion(int idsent, int idreceiver, String message, Data data) {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == idreceiver) {
                try {
                    serverThread.write("person-file" + "," + message + "," + data.getName() + "," + idsent);
//                    ObjectOutputStream os1 = new ObjectOutputStream(serverThread.getSocket().getOutputStream());
//                    os1.writeObject(data);
//                    os1.flush();
                    System.out.println("Server gui file thanh cong");
                    break;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void sendFileGroupToPersion(int idsent, int idreceiver, String message, Data data) {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == idreceiver) {
                try {
                    serverThread.write("person-filegroup" + "," + message + "," + data.getName() + "," + idsent);
//                    ObjectOutputStream os1 = new ObjectOutputStream(serverThread.getSocket().getOutputStream());
//                    os1.writeObject(data);
//                    os1.flush();
//                    System.out.println("Server gui file thanh cong");
                    break;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void refreshMessages(int id, String isGroup) throws SQLException, ClassNotFoundException {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == id) {
                try {
                    serverThread.write("refresh-messages," + isGroup);
                    break;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void deleteMessage(int id, String message) {
        for (ServerThread serverThread : Server.serverThreadBus.getListServerThreads()) {
            if (serverThread.getClientNumber() == id) {
                try {
                    serverThread.write("global-message" + "," + message);
                    break;
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void remove(int id) {
        for (int i = 0; i < Server.serverThreadBus.getLength(); i++) {
            if (Server.serverThreadBus.getListServerThreads().get(i).getClientNumber() == id) {
                Server.serverThreadBus.listServerThreads.remove(i);
            }
        }
    }
}
