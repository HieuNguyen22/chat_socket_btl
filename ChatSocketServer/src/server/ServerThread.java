/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import dao.GroupDao;
import dao.MessageDao;
import dao.UserDao;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import static java.lang.System.out;
import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.GroupLayout.Group;
import models.Data;
import models.GroupModel;
import models.Groups;
import models.MessageModel;
import models.Messages;
import models.ParticipantModel;
import models.User;
import models.UserLogin;
import models.UserModel;
import models.Users;

/**
 *
 * @author Admin
 */
public class ServerThread implements Runnable {

    private Socket socketOfServer;
    private int clientNumber;
    private BufferedReader is;
    private BufferedWriter os;
    private boolean isClosed;

    private String serverPath = "D:/files_btl_ltm/file_server/";

    private MessageDao messageDao = new MessageDao();

    public BufferedReader getIs() {
        return is;
    }

    public BufferedWriter getOs() {
        return os;
    }

    public int getClientNumber() {
        return clientNumber;
    }

    public Socket getSocket() {
        return socketOfServer;
    }

    public ServerThread(Socket socketOfServer, int clientNumber) {
        this.socketOfServer = socketOfServer;
        this.clientNumber = clientNumber;
        System.out.println("Server thread number " + clientNumber + " Started");
        isClosed = false;
    }

    @Override
    public void run() {
        try {
            // Mở luồng vào ra trên Socket tại Server.
            is = new BufferedReader(new InputStreamReader(socketOfServer.getInputStream()));
            os = new BufferedWriter(new OutputStreamWriter(socketOfServer.getOutputStream()));
            System.out.println("Khời động luông mới thành công, ID là: " + clientNumber);
            write("get-id" + "," + this.clientNumber);
            Server.serverThreadBus.sendOnlineList();
//            Server.serverThreadBus.mutilCastSend("global-message"+","+"---Client "+this.clientNumber+" đã đăng nhập---");
            String message;
            while (!isClosed) {
                message = is.readLine();
                System.out.println(message);
                if (message == null) {
                    break;
                }
                String[] messageSplit = message.split(",");
                if (messageSplit[0].equals("send-to-global")) {
                    Server.serverThreadBus.boardCast(this.getClientNumber(), "global-message" + "," + "Client " + messageSplit[2] + ": " + messageSplit[1]);
                }

                if (messageSplit[0].equals("send-to-person")) {
                    messageDao.addMessage(messageSplit[1], messageSplit[2], messageSplit[3], messageSplit[4], messageSplit[5], messageSplit[6]);

                    if (messageSplit[5].equals("0")) {
                        // Check co phai la icon hay khong
                        if (messageSplit[6].equals("0")) {
                            Server.serverThreadBus.sendMessageToPersion(Integer.parseInt(messageSplit[2]), messageSplit[7] + ": " + messageSplit[3]);
                        } else {
                            Server.serverThreadBus.sendIconToPersion(Integer.parseInt(messageSplit[2]), messageSplit[3], messageSplit[1]);
                        }
                    } else {
                        List<ParticipantModel> list = GroupDao.getParticipants(Integer.parseInt(messageSplit[2]));
                        for (ParticipantModel x : list) {
                            if (x.getUserId() != Integer.parseInt(messageSplit[1])) {
                                System.out.println("gui rieng 1 thang " + x.getUserId());

                                // Check co phai la icon hay khong
                                if (messageSplit[6].equals("0")) {
                                    Server.serverThreadBus.sendMessageToPersion(x.getUserId(), messageSplit[7] + ": " + messageSplit[3]);
                                } else {
                                    Server.serverThreadBus.sendIconToPersion(x.getUserId(), messageSplit[3], messageSplit[1]);
                                }
                            }
                        }
                    }
                }

                if (messageSplit[0].equals("LOGIN")) {
                    System.out.println("DA NHAN MESSAGE LOGIN");
                    ServerDao dao = new ServerDao();

                    if (dao.login(new UserLogin(messageSplit[1], messageSplit[2]))) {
                        System.out.println("DA CHECK MAT KHAU THANH CONG");

                        write("get-id" + "," + dao.getIdUser(messageSplit[1]));
                        this.clientNumber = dao.getIdUser(messageSplit[1]);

                        write("login ok");
                        UserDao userDao = new UserDao();

                        dao.updateCheck("1", this.clientNumber);

                        Users x = new Users(userDao.getUsers());
                        ObjectOutputStream oos = new ObjectOutputStream(socketOfServer.getOutputStream());
                        oos.writeObject(x);
                        oos.flush();

                    } else {
                        write("login failed");
                    }
                }

                if (messageSplit[0].equals("request-groups")) {
                    write("send-groups");
                    GroupDao groupDao = new GroupDao();
                    Groups listGroup = new Groups(groupDao.getGroups(Integer.parseInt(messageSplit[1])));

                    ObjectOutputStream oosGroup = new ObjectOutputStream(socketOfServer.getOutputStream());
                    oosGroup.writeObject(listGroup);
                    oosGroup.flush();
                }

                if (messageSplit[0].equals("newgroup")) {
                    ObjectInputStream in1 = new ObjectInputStream(socketOfServer.getInputStream());
                    GroupModel group = (GroupModel) in1.readObject();
                    MessageDao.newGroup(group.getName(), group.getMember());

                    for (UserModel x : group.getMember()) {
                        System.out.println("Gui rieng 1 thang " + x.getId());

                        Server.serverThreadBus.sendNotiNewGroup(x.getId());

                    }
                }

                if (messageSplit[0].equals("delete-message")) {
                    System.out.println("start");
                    messageDao.deleteMessage(messageSplit[1]);
                    System.out.println("end");

                    if (messageSplit[4].equals("0")) {
                        Server.serverThreadBus.refreshMessages(Integer.parseInt(messageSplit[3]), "0");
                    } else {
                        List<ParticipantModel> list = GroupDao.getParticipants(Integer.parseInt(messageSplit[3]));
                        for (ParticipantModel x : list) {
                            if (x.getUserId() != Integer.parseInt(messageSplit[2])) {
                                System.out.println("Xoa rieng cho tung thang " + x.getUserId());

                                Server.serverThreadBus.refreshMessages(x.getUserId(), "1");
                            }
                        }
                    }
                }

                if (messageSplit[0].equals("getmessage")) {
                    System.out.println("da gui message");
                    write("message");
                    MessageDao messdao = new MessageDao();
                    Messages x = new Messages();

                    // Set default list = []
                    List<MessageModel> tmpList = new ArrayList<>();
                    x.setListmessages(tmpList);

                    // Fetch data
                    x.setListmessages(messdao.getMessagesOfBoxChat(Integer.parseInt(messageSplit[1]), Integer.parseInt(messageSplit[2]), Integer.parseInt(messageSplit[3])));
                    System.out.println(messageSplit[1] + " - " + messageSplit[2] + " - " + messageSplit[3]);
                    ObjectOutputStream oos = new ObjectOutputStream(socketOfServer.getOutputStream());

                    oos.writeObject(x);

                    oos.flush();
                }

                if (messageSplit[0].equals("SIGNUP")) {
                    ServerDao dao = new ServerDao();
                    if (dao.addUser(new User(messageSplit[1], messageSplit[2], messageSplit[3], messageSplit[4]))) {
                        write("sign up ok");
                    } else {
                        write("sign up failed");
                    }
                }
                if (messageSplit[0].equals("SENDFILE")) {
                    try {
                        ObjectInputStream in1 = new ObjectInputStream(socketOfServer.getInputStream());
                        Data data = (Data) in1.readObject();

                        File myObj = new File(serverPath + data.getName());

                        myObj.createNewFile();
                        OutputStream out = new FileOutputStream(serverPath + data.getName());
                        out.write(data.getFile());
                        out.close();
                        messageDao.addFileMessage(messageSplit[1], messageSplit[2], serverPath + data.getName(), messageSplit[4], messageSplit[5], messageSplit[6]);

                        if (messageSplit[5].equals("0")) {
                            Server.serverThreadBus.sendFileToPersion(Integer.parseInt(messageSplit[1]), Integer.parseInt(messageSplit[2]), data.getName(), data);
                        } else {
                            List<ParticipantModel> list = GroupDao.getParticipants(Integer.parseInt(messageSplit[2]));
                            for (ParticipantModel x : list) {
                                if (x.getUserId() != Integer.parseInt(messageSplit[1])) {
                                    System.out.println("gui rieng 1 thang " + x.getUserId());
                                    Server.serverThreadBus.sendFileGroupToPersion(Integer.parseInt(messageSplit[1]), x.getUserId(), data.getName(), data);
                                }
                            }

                            System.out.println("nhan file thanh cong");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                if (messageSplit[0].equals("Get File")) {
                    try {
                        File myObj = new File("D:/files_btl_ltm/file_server/" + messageSplit[1]);

                        if (myObj.exists()) {
                            write("get-file");
                            Data data = new Data();
                            FileInputStream in = new FileInputStream(myObj);
                            int length = in.available();
                            byte b[] = new byte[length];
                            in.read(b);
                            data.setFile(b);
                            data.setName(messageSplit[1]);
                            ObjectOutputStream out1 = new ObjectOutputStream(socketOfServer.getOutputStream());
                            out1.writeObject(data);
                            out1.flush();

//                          messageDao.addFileMessage(messageSplit[1], messageSplit[2],"/Users/unin/Desktop/file"+data.getName(), messageSplit[4], messageSplit[5], messageSplit[6]);
//                          Server.serverThreadBus.sendFileToPersion(Integer.parseInt(messageSplit[2]), data.getName() , data);
                            System.out.println("gui file thanh cong");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }
        } catch (IOException e) {
            isClosed = true;
            Server.serverThreadBus.remove(clientNumber);
            System.out.println(this.clientNumber + " đã thoát");

            ServerDao dao = new ServerDao();
            try {
                dao.updateCheck("0", this.clientNumber);
            } catch (Exception x) {
            }

            Server.serverThreadBus.sendOnlineList();
            Server.serverThreadBus.mutilCastSend("global-message" + "," + "---Client " + this.clientNumber + " đã thoát---");

        } catch (SQLException ex) {
            Logger.getLogger(ServerThread.class
                    .getName()).log(Level.SEVERE, null, ex);

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ServerThread.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void write(String message) throws IOException {
        os.write(message);
        os.newLine();
        os.flush();
    }

}
