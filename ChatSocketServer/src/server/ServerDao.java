/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import models.User;
import models.UserLogin;

/**
 *
 * @author Admin
 */
public class ServerDao {

    private Connection conn;
    private ResultSet rs;

    public ServerDao() {
        try {
//System.out.println("ii");
            Class.forName("com.mysql.cj.jdbc.Driver");

            conn = DriverManager.getConnection("jdbc:mysql://101.99.6.31:11122/ltm_chatapp?useUnicode=true&characterEndcoding=utf-8", "ltm", "Aladin@123");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean addUser(User s) {
        String sql = "insert into user(fullname, username, phoneNum, password) values (?,?,?,?)";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, s.getHoVaTen());
            ps.setString(2, s.getTenDangNhap());
            ps.setString(3, s.getSoDT());
            ps.setString(4, s.getMatKhau());
            ps.executeUpdate();
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public boolean login(UserLogin s) {
        String sql = "select * from user where username=? and password=?";
        UserLogin n = null;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, s.getUsername());
            ps.setString(2, s.getPassword());
            rs = ps.executeQuery();
            while (rs.next()) {
                n = new UserLogin(rs.getString(1), rs.getString(2));
            }
            if (n == null) {
                return false;
            } else {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public int getIdUser(String username) {
        String sql = "select id from user where username=?";
        int n = -1;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                n = rs.getInt("id");
                System.out.println(n + " id db");
            }
            return n;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return n;
    }

    public void updateCheck(String s, int id) throws SQLException {
        String sql = "update user set checkOnline=? where id=?";

        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, s);
        ps.setInt(2, id);
        ps.executeUpdate();

    }

    public static void main(String[] args) {
        ServerDao f = new ServerDao();

    }
}
