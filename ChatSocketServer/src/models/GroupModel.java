/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author unin
 */
public class GroupModel implements Serializable {

    private int id;
    private String name;
    private List<UserModel> member;

    private static final long serialVersionUID = 1234567L;

    public GroupModel(int id, String name, List<UserModel> member) {
        this.id = id;
        this.name = name;
        this.member = member;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UserModel> getMember() {
        return member;
    }

    public void setMember(List<UserModel> member) {
        this.member = member;
    }


}
