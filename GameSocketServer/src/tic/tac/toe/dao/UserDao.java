/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package tic.tac.toe.dao;

import java.sql.*;
import tic.tac.toe.dto.MatchInfoDTO;
import models.User;

/**
 *
 * @author 84978
 */
public class UserDao {
    public static Connection connection = DBConnection.getInstant();
    
    public static User login(String username, String password) {
        String sql = "select * from user where username = ? and password = ?";
        User user = null; 
        try{
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                  user = new User();
                  user.setHoVaTen(rs.getNString("fullname"));
                  user.setTenDangNhap(rs.getNString("username"));
                  user.setMatKhau(rs.getString("password"));   
                  user.setSoDT(rs.getString("phoneNum"));  
            }
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return user;
    }
    
    public static MatchInfoDTO getMatchInfo(String myUsername, String partnerUsername) {
        String sql = ""
                + "select id, me, partner, numWin, numLost, numDraw "
                + "    from match_info "
                + "    where (me = ? and partner = ?) or (me = ? and partner = ?);";
        
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, myUsername);
            stmt.setString(2, partnerUsername);
            stmt.setString(3, partnerUsername);
            stmt.setString(4, myUsername);
             
            ResultSet rs = stmt.executeQuery();
            
            if (rs.next()) {
                return new MatchInfoDTO(
                        rs.getString("me"), 
                        rs.getString("partner"), 
                        rs.getInt("numWin"),
                        rs.getInt("numLost"),
                        rs.getInt("numDraw")
                );
            }
            stmt.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        MatchInfoDTO emptyMatch = new MatchInfoDTO(myUsername, partnerUsername, 0, 0, 0);
        saveMatchInfo(emptyMatch);
        return emptyMatch;
    }
    
    public static boolean deleteMatchInfo(String myUsername, String partnerUsername) {
    
         String sql = "delete * from match_info "
                + "   where (me = ? and partner = ?) or (me = ? and partner = ?0);";
        
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, myUsername);
            stmt.setString(2, partnerUsername);
            stmt.setString(3, partnerUsername);
            stmt.setString(4, myUsername);
             
            int row = stmt.executeUpdate();
            
            if (row > 0) {
                return true;
            }
            stmt.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        
        return false;
    }
    
    public static boolean incrementWin(String myUsername, String partnerUsername) {
        String sql1 = "update match_info set numWin = numWin + 1 "
                + "    where (me = ? and partner = ?);";
        
        String sql2 = "update match_info set numLost = numLost + 1 "
                + "    where (me = ? and partner = ?);";
        
        try {
            PreparedStatement stmt = connection.prepareStatement(sql1);
            PreparedStatement stmt2 = connection.prepareStatement(sql2);
            stmt.setString(1, myUsername);
            stmt.setString(2, partnerUsername);
            stmt2.setString(1, partnerUsername);
            stmt2.setString(2, myUsername);
             
            int row = stmt.executeUpdate() + stmt2.executeUpdate();
            
            if (row > 0) {
                return true;
            }
            stmt.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        return false;
    }
    
    public static boolean incrementDraw(String myUsername, String partnerUsername) {
        String sql = "update match_info set numDraw = numDraw + 1 "
                + "    where (me = ? and partner = ?) or (me = ? and partner = ?);";
        
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, myUsername);
            stmt.setString(2, partnerUsername);
            stmt.setString(3, partnerUsername);
            stmt.setString(4, myUsername);
             
            int row = stmt.executeUpdate();
            
            if (row > 0) {
                return true;
            }
            stmt.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        
        return false;
    }
        
    public static MatchInfoDTO saveMatchInfo(MatchInfoDTO matchInfoDTO) {
        String sql = "insert into match_info (me, partner, numWin, numLost, numDraw)"
                + "    values (?, ?, ?, ?, ?);";
        
        try {
            PreparedStatement stmt = connection.prepareStatement(sql);
            stmt.setString(1, matchInfoDTO.getMyUsername());
            stmt.setString(2, matchInfoDTO.getPartnerUsername());
            stmt.setInt(3, matchInfoDTO.getNumWin());
            stmt.setInt(4, matchInfoDTO.getNumLost());
            stmt.setInt(5, matchInfoDTO.getNumDraw());
             
            int row = stmt.executeUpdate();
            
            if (row > 0) {
                return matchInfoDTO;
            }
            stmt.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        } 
        
        return null;
    }
    
    public static void main(String[] args) throws InterruptedException{

        int count=0;
        for (float y=2.5f;y>-2.0f;y-=0.12f){
            for (float x=-2.3f;x<2.3f;x+=0.041f){
                float a=x*x+y*y-4f;
                if ((a*a*a-x*x*y*y*y)<-0.0f){
                    String str="CƯỜNG YÊU TRANG!";
                    int num=count%str.length();
                    System.err.print(str.charAt(num));
                    count++;
                }else{
                    System.err.print(" ");
                }
            }
            System.err.println();
            Thread.sleep(100);
        }

    }
}
