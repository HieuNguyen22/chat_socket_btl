/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package client;

import client.Login;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

import controller.GameClientController;
import models.Data;
import models.GroupModel;
import models.MessageModel;
import models.UserModel;
import utils.MouseLiberalAdapter;

/**
 *
 * @author Avada Kedavra
 */
public class ClientF extends javax.swing.JFrame {
    public GameClientController clientController;
    private String partnerUsername;
    private int myId;
    public int clientId;
    public int isGroup = 0;
    private String username;
    public Thread thread;
    public BufferedWriter os;
    private BufferedReader is;
    private Socket socketOfClient;

    public javax.swing.JPanel content;
    
    private String clientPath = "D:/files_btl_ltm/file_client/";

    public List<GroupModel> groups = new ArrayList<>();
    public List<UserModel> users = new ArrayList<>();
    private List<UserModel> friends = new ArrayList<>();
    public List<MessageModel> messages = new ArrayList<>();

    NewGroup groupF;

    public int getMyId() {
        return myId;
    }

    public ClientF() throws SQLException, ClassNotFoundException {
        initComponents();
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setVisible(true);

        content = new JPanel();

        btnCrying.setIcon(new ImageIcon(ClientF.class.getResource("/images/crying.png")));
        btnHeartEye.setIcon(new ImageIcon(ClientF.class.getResource("/images/heart_eye.png")));
        btnSad.setIcon(new ImageIcon(ClientF.class.getResource("/images/sad.png")));
        btnSmileBig.setIcon(new ImageIcon(ClientF.class.getResource("/images/smile_big.png")));
        btnSmileCry.setIcon(new ImageIcon(ClientF.class.getResource("/images/smile_cry.png")));
        btnScared.setIcon(new ImageIcon(ClientF.class.getResource("/images/scared.png")));

        System.out.println("chat main");

//        setUpSocket();
//        setDefaultValues();
//        catchEventOnline();
    }

    private void createMessage(MessageModel message) {
        JLabel text = new JLabel();
        if (message.getSenderId() == myId) {
            text.setText("Ban: " + message.getText());
        } else {
            List<UserModel> tempUser = users
                    .stream()
                    .filter(q -> q.getId() == message.getSenderId())
                    .collect(Collectors.toList());
            text.setText(tempUser.get(0).getFullname() + ": " + message.getText());
        }
//        System.out.println(message.getText());

        JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        tmpPanel.add(text);

        Border border = tmpPanel.getBorder();
        Border margin = new EmptyBorder(10, 10, 10, 10);
        tmpPanel.setBorder(new CompoundBorder(border, margin));

        content.add(tmpPanel);
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

        jMessageScrollPanel.setViewportView(content);

        text.addMouseListener(new MouseLiberalAdapter() {
            @Override
            public void mouseLiberalClick(MouseEvent e) {
                if (message.getSenderId() == myId) {
                    Object[] options = {"Dong y", "Huy"};
                    int result = JOptionPane.showOptionDialog(null,
                            "Ban co chac chan muon xoa tin nhan?",
                            "Canh bao",
                            JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[0]);
                    if (result == JOptionPane.YES_OPTION) {
                        System.out.println("Bạn chọn: Yes");
                        content.remove(tmpPanel);
                        jMessageScrollPanel.setViewportView(content);

                        try {
                            if (isGroup == 0) {
                                write("delete-message" + "," + message.getId() + "," + message.getSenderId() + "," + message.getReceiverId() + "," + 0);
                            } else {
                                write("delete-message" + "," + message.getId() + "," + message.getSenderId() + "," + message.getReceiverId() + "," + 1);
                            }
                        } catch (IOException ex) {
                            Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else if (result == JOptionPane.NO_OPTION) {
                        System.out.println("Bạn chọn : No");
                    } else if (result == JOptionPane.CANCEL_OPTION) {
                        System.out.println("Ban chon cancel");
                    } else {
                        System.out.println("Chưa ");
                    }
                } else {
                }
            }
        });
    }

    public void createIconMessage(int iconId, int idSender) throws IOException {
        String path = "";

        // Check icon
        switch (iconId) {
            case 1:
                path = "/images/smile_cry.png";
                break;
            case 2:
                path = "/images/smile_big.png";
                break;
            case 3:
                path = "/images/scared.png";
                break;
            case 4:
                path = "/images/sad.png";
                break;
            case 5:
                path = "/images/heart_eye.png";
                break;
            case 6:
                path = "/images/crying.png";
                break;
            default:
                path = "/images/smile_cry.png";
                break;
        }

        BufferedImage myPicture = ImageIO.read(ClientF.class.getResource(path));
        JLabel iconLabel = new JLabel(new ImageIcon(myPicture));

        // Check sender
        String senderName = "";

        if (idSender == myId) {
            senderName = "Ban:";
        } else {
            List<UserModel> tempUser = users
                    .stream()
                    .filter(q -> q.getId() == idSender)
                    .collect(Collectors.toList());
            senderName = tempUser.get(0).getFullname() + ":";
        }

        // Set up view
        JLabel textLabel = new JLabel();
        textLabel.setText(senderName);

        JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        tmpPanel.add(textLabel);
        tmpPanel.add(iconLabel);

        Border border = tmpPanel.getBorder();
        Border margin = new EmptyBorder(10, 10, 10, 10);
        tmpPanel.setBorder(new CompoundBorder(border, margin));

        content.add(tmpPanel);
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

        jMessageScrollPanel.setViewportView(content);
    }

    public void createFileMessage(MessageModel message) {
        JButton btn = new JButton();
        if (message.getSenderId() == myId) {
            btn.setText("Ban: " + message.getText());
        } else {
            List<UserModel> tempUser = users
                    .stream()
                    .filter(q -> q.getId() == message.getSenderId())
                    .collect(Collectors.toList());
            btn.setText(tempUser.get(0).getFullname() + ": " + message.getText());
        }
//        System.out.println(message.getText());

        JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        tmpPanel.add(btn);

        Border border = tmpPanel.getBorder();
        Border margin = new EmptyBorder(10, 10, 10, 10);
        tmpPanel.setBorder(new CompoundBorder(border, margin));

        content.add(tmpPanel);
        content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

        jMessageScrollPanel.setViewportView(content);

        scrollToBottom();

        btn.addMouseListener(new MouseLiberalAdapter() {

            @Override
            public void mouseLiberalClick(MouseEvent e) {

                if (message.getSenderId() == myId && e.getClickCount() == 2) {
                    Object[] options = {"Dong y", "Huy"};
                    int result = JOptionPane.showOptionDialog(null,
                            "Ban co chac chan muon xoa tin nhan?",
                            "Canh bao",
                            JOptionPane.YES_NO_CANCEL_OPTION,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            options,
                            options[0]);
                    if (result == JOptionPane.YES_OPTION) {
                        System.out.println("Bạn chọn: Yes");
                        content.remove(btn);
                        jMessageScrollPanel.setViewportView(content);

                        try {
                            write("delete-message" + "," + message.getId() + "," + message.getSenderId() + "," + message.getReceiverId());
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                    } else if (result == JOptionPane.NO_OPTION) {
                        System.out.println("Bạn chọn : No");
                    } else if (result == JOptionPane.CANCEL_OPTION) {
                        System.out.println("Ban chon cancel");
                    } else {
                        System.out.println("Chưa ");
                    }
                } else {
                    if (e.getClickCount() == 1) {

                        Desktop d = null;
                        if (Desktop.isDesktopSupported()) {
                            File f = new File(clientPath + message.getText());
                            System.out.println(message.getText());
                            if (!f.exists()) {
                                try {
                                    System.out.println("Getfile demo");
                                    os.write("Get File," + message.getText());
                                    os.newLine();
                                    os.flush();
                                    System.out.println("loading file");
                                    while (!f.exists()) {
                                        System.out.print(".");
                                    }
////                                                    ObjectInputStream ois = new ObjectInputStream(socketOfClient.getInputStream());
////                                                    Data data = (Data) ois.readObject();
////                                                    File myObj = new File("/Users/unin/Desktop/fileclient/"+data.getName());
////                        
////                                                    myObj.createNewFile();
////                                                    OutputStream  out = new FileOutputStream("/Users/unin/Desktop/fileclient/"+data.getName());
////                                                    out.write(data.getFile());
////                                                    out.close();
//                                                   
                                } catch (IOException ex) {
                                    Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                            try {
                                Desktop.getDesktop().open(f);
                            } catch (IOException ex) {
                                Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            }
        });
    }

    public void refreshMessages() throws SQLException, ClassNotFoundException, IOException {
        int tmp = messages.size();
        System.out.println("FETCHED MESSAGE SIZE: " + messages.size());
        // Set messages
        content.removeAll();
        jMessageScrollPanel.setViewportView(content);

        for (MessageModel message : messages) {

            // Check phai la file khong?
            if (message.getIsFile() == 1) {
                String[] x = message.getText().split("/");
                message.setText(x[x.length - 1]);
                createFileMessage(message);
            }
            // Check phai la icon khong?
            if (message.getIsIcon() == 1) {
                createIconMessage(Integer.parseInt(message.getText()), message.getSenderId());
            }
            // Check phai la message khong?
            if (message.getIsIcon() == 0 && message.getIsFile() == 0) {
                createMessage(message);
            }
        }
        scrollToBottom();
    }

    public void setSocket(Socket socket, BufferedWriter os, BufferedReader is) {
        this.socketOfClient = socket;
        this.os = os;
        this.is = is;
    }

    public void jMessageScrollPanelsetViewportView(javax.swing.JPanel content) {
        jMessageScrollPanel.setViewportView(content);
    }

    public void scrollToBottom() {
        JScrollBar vertical = jMessageScrollPanel.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
    }

    public void setDefaultValues() throws SQLException, ClassNotFoundException, IOException {
        jMessageScrollPanel.setSize(700, 440);

        // Set list friends
        System.out.println("My Id: " + myId);
        String[] data = new String[users.size() - 1];
        int tmpCount = 0;

        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId() == myId) {
                continue;
            } else {
                friends.add(users.get(i));

                if (users.get(i).getCheckOnline().equals("1")) {
                    data[tmpCount++] = users.get(i).getFullname() + " đang online\n";

                } else {
                    data[tmpCount++] = users.get(i).getFullname() + " đang offline\n";

                }
            }
        }

        jListUsers.setListData(data);
        jListUsers.setBorder(new EmptyBorder(5, 5, 5, 5));
        jListUsers.setBackground(Color.WHITE);
        jListUsers.setForeground(Color.RED);
        jListUsers.setFont(new Font("Segoe UI", Font.PLAIN, 15));
        jListUsers.setBounds(10, 20, 577, 332);
        jListUsers.setSelectedIndex(0);

        System.out.println("DATA FRIENDS: " + data.length);

        // Set list groups
        String[] dataGroups = new String[groups.size()];

        for (int i = 0; i < groups.size(); i++) {
            dataGroups[i] = "Nhóm: " + groups.get(i).getName();
        }

        jListGroups.setListData(dataGroups);
        jListGroups.setBorder(new EmptyBorder(5, 5, 5, 5));
        jListGroups.setBackground(Color.WHITE);
        jListGroups.setForeground(Color.GREEN);
        jListGroups.setFont(new Font("Segoe UI", Font.PLAIN, 15));
        jListGroups.setBounds(10, 20, 577, 332);
//        jListGroups.setSelectedIndex(0);

        System.out.println("DATA GROUP: " + dataGroups.length);

        // Set client
        lbName.setText(friends.get(0).getFullname());
        clientId = friends.get(0).getId();
        partnerUsername = friends.get(0).getUsername();

        // Set messages
        os.write("getmessage," + myId + "," + clientId + ",0");
        os.newLine();
        os.flush();
    }

    public void catchEventOnline() {
        jListUsers.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                isGroup = 0;
                btnGame.setVisible(true);
                jListGroups.clearSelection();
                String title = jListUsers.getSelectedValue();
                String[] parts = title.split(" ");
                List<UserModel> tempUser = users
                        .stream()
                        .filter(q -> q.getFullname().equals(parts[0]))
                        .collect(Collectors.toList());

                String name = tempUser.get(0).getFullname();
                clientId = tempUser.get(0).getId();
                partnerUsername = friends.get(0).getUsername();
                lbName.setText(name);
                System.out.println("Client Id: " + clientId + " - Client Name: " + name);

                try {
                    os.write("getmessage," + myId + "," + clientId + ",0");
                    os.newLine();
                    os.flush();
                } catch (IOException ex) {
                    Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void catchEventGroup() {
        jListGroups.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent arg0) {
                isGroup = 1;
                btnGame.setVisible(false);
                jListUsers.clearSelection();
                String title = jListGroups.getSelectedValue();
                String[] parts = title.split(" ");
                String nameGroup = title.replace(parts[0] + " ", "");
                System.out.println("NAME GROUP: " + nameGroup);

                List<GroupModel> tempGroup = groups
                        .stream()
                        .filter(q -> q.getName().equals(nameGroup))
                        .collect(Collectors.toList());

                clientId = tempGroup.get(0).getId();

                System.out.println("Group Id: " + tempGroup.get(0).getId() + " - Client Name: " + tempGroup.get(0).getName());
                lbName.setText(tempGroup.get(0).getName());

                try {
                    os.write("getmessage," + myId + "," + clientId + ",1");
                    os.newLine();
                    os.flush();
                } catch (IOException ex) {
                    Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    public void write(String message) throws IOException {
        os.write(message);
        os.newLine();
        os.flush();
    }

    private void setIDTitle() {
        this.setTitle("Client " + this.myId);
    }

    public void setID(int id) {
        this.myId = id;
    }

    public void setUsername(String username) {
        txtUsername.setText(username);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        relativePanel = new javax.swing.JPanel();
        recentLabel = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanelUser = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jListUsers = new javax.swing.JList<>();
        jPanelGroup = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jListGroups = new javax.swing.JList<>();
        chatPanel = new javax.swing.JPanel();
        jMessageScrollPanel = new javax.swing.JScrollPane();
        lbName = new javax.swing.JLabel();
        lbStatus = new javax.swing.JLabel();
        tfInput = new javax.swing.JTextField();
        btnSend = new javax.swing.JButton();
        btnSmileCry = new javax.swing.JButton();
        btnSmileBig = new javax.swing.JButton();
        btnScared = new javax.swing.JButton();
        btnSad = new javax.swing.JButton();
        btnHeartEye = new javax.swing.JButton();
        btnFile = new javax.swing.JButton();
        btnCrying = new javax.swing.JButton();
        btnGame = new javax.swing.JButton();
        btnGroup = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtUsername = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setText("MESSAGING");

        relativePanel.setBackground(new java.awt.Color(255, 255, 255));
        relativePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        relativePanel.setForeground(new java.awt.Color(255, 255, 255));

        recentLabel.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        recentLabel.setText("Recent");

        jTabbedPane1.setToolTipText("");

        jListUsers.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jScrollPane3.setViewportView(jListUsers);

        javax.swing.GroupLayout jPanelUserLayout = new javax.swing.GroupLayout(jPanelUser);
        jPanelUser.setLayout(jPanelUserLayout);
        jPanelUserLayout.setHorizontalGroup(
            jPanelUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
        );
        jPanelUserLayout.setVerticalGroup(
            jPanelUserLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Nguoi dùng", jPanelUser);

        jListGroups.setModel(new javax.swing.AbstractListModel<String>() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public String getElementAt(int i) { return strings[i]; }
        });
        jListGroups.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(jListGroups);

        javax.swing.GroupLayout jPanelGroupLayout = new javax.swing.GroupLayout(jPanelGroup);
        jPanelGroup.setLayout(jPanelGroupLayout);
        jPanelGroupLayout.setHorizontalGroup(
            jPanelGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 271, Short.MAX_VALUE)
        );
        jPanelGroupLayout.setVerticalGroup(
            jPanelGroupLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Nhóm", jPanelGroup);

        javax.swing.GroupLayout relativePanelLayout = new javax.swing.GroupLayout(relativePanel);
        relativePanel.setLayout(relativePanelLayout);
        relativePanelLayout.setHorizontalGroup(
            relativePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, relativePanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(recentLabel)
                .addGap(111, 111, 111))
            .addGroup(relativePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );
        relativePanelLayout.setVerticalGroup(
            relativePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(relativePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(recentLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Nguoi dùng");

        chatPanel.setBackground(new java.awt.Color(255, 255, 255));
        chatPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        chatPanel.setForeground(new java.awt.Color(255, 255, 255));

        lbName.setFont(new java.awt.Font("Segoe UI", 1, 16)); // NOI18N
        lbName.setText("Nguyen Trung Hieu");

        lbStatus.setBackground(new java.awt.Color(0, 255, 0));
        lbStatus.setFont(new java.awt.Font("Segoe UI", 1, 13)); // NOI18N
        lbStatus.setForeground(new java.awt.Color(0, 170, 0));
        lbStatus.setText("Online");

        tfInput.setToolTipText("");
        tfInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfInputActionPerformed(evt);
            }
        });

        btnSend.setText("Send");
        btnSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendActionPerformed(evt);
            }
        });

        btnSmileCry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSmileCryActionPerformed(evt);
            }
        });

        btnSmileBig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSmileBigActionPerformed(evt);
            }
        });

        btnScared.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnScaredActionPerformed(evt);
            }
        });

        btnSad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSadActionPerformed(evt);
            }
        });

        btnHeartEye.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHeartEyeActionPerformed(evt);
            }
        });

        btnFile.setText("File");
        btnFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFileActionPerformed(evt);
            }
        });

        btnCrying.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCryingActionPerformed(evt);
            }
        });

        btnGame.setText("Choi Game");
        btnGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout chatPanelLayout = new javax.swing.GroupLayout(chatPanel);
        chatPanel.setLayout(chatPanelLayout);
        chatPanelLayout.setHorizontalGroup(
            chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(chatPanelLayout.createSequentialGroup()
                .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(chatPanelLayout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbStatus)
                            .addComponent(lbName))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnGame))
                    .addGroup(chatPanelLayout.createSequentialGroup()
                        .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfInput, javax.swing.GroupLayout.PREFERRED_SIZE, 617, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(chatPanelLayout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(btnSmileCry, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(btnSmileBig, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(24, 24, 24)
                                .addComponent(btnScared, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(btnSad, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(btnHeartEye, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(btnCrying, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSend, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chatPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jMessageScrollPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 700, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        chatPanelLayout.setVerticalGroup(
            chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, chatPanelLayout.createSequentialGroup()
                .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(chatPanelLayout.createSequentialGroup()
                        .addComponent(lbName)
                        .addGap(1, 1, 1)
                        .addComponent(lbStatus))
                    .addGroup(chatPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnGame)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jMessageScrollPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 440, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnFile, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSmileCry, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSmileBig, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnScared, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnSad, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnHeartEye, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnCrying, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(chatPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnSend, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
                    .addComponent(tfInput))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jMessageScrollPanel.getAccessibleContext().setAccessibleDescription("");

        btnGroup.setText("Tao nhom");
        btnGroup.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGroupActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("USER: ");

        txtUsername.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        txtUsername.setForeground(new java.awt.Color(0, 204, 0));
        txtUsername.setText("user");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnGroup)
                        .addGap(56, 56, 56)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtUsername, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(relativePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(chatPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(145, 145, 145)
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtUsername, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(btnGroup)))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(relativePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(chatPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tfInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfInputActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfInputActionPerformed

    private void btnSmileCryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSmileCryActionPerformed
        BufferedImage myPicture;

        List<UserModel> tempUser = users
                .stream()
                .filter(q -> q.getId() == myId)
                .collect(Collectors.toList());

        try {
            myPicture = ImageIO.read(ClientF.class.getResource("/images/smile_cry.png"));
            JLabel iconLabel = new JLabel(new ImageIcon(myPicture));

            JLabel textLabel = new JLabel();
            textLabel.setText("Ban:");

            JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            tmpPanel.add(textLabel);
            tmpPanel.add(iconLabel);

            Border border = tmpPanel.getBorder();
            Border margin = new EmptyBorder(10, 10, 10, 10);
            tmpPanel.setBorder(new CompoundBorder(border, margin));

            content.add(tmpPanel);
            content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

            jMessageScrollPanel.setViewportView(content);

            scrollToBottom();

            if (isGroup == 0) {
                write("send-to-person" + "," + myId + "," + clientId + ",1," + LocalDateTime.now() + "," + 0 + "," + 1 + "," + tempUser.get(0).getFullname());
            } else {
                write("send-to-person" + "," + myId + "," + clientId + ",1," + LocalDateTime.now() + "," + 1 + "," + 1 + "," + tempUser.get(0).getFullname());
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSmileCryActionPerformed

    private void btnSmileBigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSmileBigActionPerformed
        BufferedImage myPicture;

        List<UserModel> tempUser = users
                .stream()
                .filter(q -> q.getId() == myId)
                .collect(Collectors.toList());

        try {
            myPicture = ImageIO.read(ClientF.class.getResource("/images/smile_big.png"));
            JLabel iconLabel = new JLabel(new ImageIcon(myPicture));

            JLabel textLabel = new JLabel();
            textLabel.setText("Ban:");

            JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            tmpPanel.add(textLabel);
            tmpPanel.add(iconLabel);

            Border border = tmpPanel.getBorder();
            Border margin = new EmptyBorder(10, 10, 10, 10);
            tmpPanel.setBorder(new CompoundBorder(border, margin));

            content.add(tmpPanel);
            content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

            jMessageScrollPanel.setViewportView(content);

            scrollToBottom();

            if (isGroup == 0) {
                write("send-to-person" + "," + myId + "," + clientId + ",2," + LocalDateTime.now() + "," + 0 + "," + 1 + "," + tempUser.get(0).getFullname());
            } else {
                write("send-to-person" + "," + myId + "," + clientId + ",2," + LocalDateTime.now() + "," + 1 + "," + 1 + "," + tempUser.get(0).getFullname());
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSmileBigActionPerformed

    private void btnSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendActionPerformed
        String messageContent = tfInput.getText();

        List<UserModel> tempUser = users
                .stream()
                .filter(q -> q.getId() == myId)
                .collect(Collectors.toList());

        if (messageContent.isEmpty()) {
            JOptionPane.showMessageDialog(rootPane, "Bạn chưa nhập tin nhắn");
            return;
        }

        try {
            if (isGroup == 0) {
                write("send-to-person" + "," + myId + "," + clientId + "," + messageContent + "," + LocalDateTime.now() + "," + 0 + "," + 0 + "," + tempUser.get(0).getFullname());

                try {
                    os.write("getmessage," + myId + "," + clientId + ",0");
                    os.newLine();
                    os.flush();
                } catch (IOException ex) {
                    Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                write("send-to-person" + "," + myId + "," + clientId + "," + messageContent + "," + LocalDateTime.now() + "," + 1 + "," + 0 + "," + tempUser.get(0).getFullname());

                try {
                    os.write("getmessage," + myId + "," + clientId + ",1");
                    os.newLine();
                    os.flush();
                } catch (IOException ex) {
                    Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(rootPane, "Có lỗi xảy ra");
        }

        tfInput.setText("");

    }//GEN-LAST:event_btnSendActionPerformed

    private void btnCryingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCryingActionPerformed
        BufferedImage myPicture;

        List<UserModel> tempUser = users
                .stream()
                .filter(q -> q.getId() == myId)
                .collect(Collectors.toList());

        try {
            myPicture = ImageIO.read(ClientF.class.getResource("/images/crying.png"));
            JLabel iconLabel = new JLabel(new ImageIcon(myPicture));

            JLabel textLabel = new JLabel();
            textLabel.setText("Ban:");

            JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            tmpPanel.add(textLabel);
            tmpPanel.add(iconLabel);

            Border border = tmpPanel.getBorder();
            Border margin = new EmptyBorder(10, 10, 10, 10);
            tmpPanel.setBorder(new CompoundBorder(border, margin));

            content.add(tmpPanel);
            content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

            jMessageScrollPanel.setViewportView(content);

            scrollToBottom();

            if (isGroup == 0) {
                write("send-to-person" + "," + myId + "," + clientId + ",6," + LocalDateTime.now() + "," + 0 + "," + 1 + "," + tempUser.get(0).getFullname());
            } else {
                write("send-to-person" + "," + myId + "," + clientId + ",6," + LocalDateTime.now() + "," + 1 + "," + 1 + "," + tempUser.get(0).getFullname());
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnCryingActionPerformed

    private void btnGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGameActionPerformed
//        JScrollBar vertical = jMessageScrollPanel.getVerticalScrollBar();
//        vertical.setValue(vertical.getMaximum());
        this.clientController.startGame(this.partnerUsername);
    }//GEN-LAST:event_btnGameActionPerformed

    private void btnFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFileActionPerformed
        // TODO add your handling code here:
        try {
            JFileChooser ch = new JFileChooser();
            int c = ch.showOpenDialog(this);
            if (c == JFileChooser.APPROVE_OPTION) {
                File f = ch.getSelectedFile();
                System.out.println();
                FileInputStream in = new FileInputStream(f);
                int length = in.available();
                byte b[] = new byte[length];
                in.read(b);
                Data data = new Data();
                data.setName(f.getName());
                data.setFile(b);
                List<UserModel> tempUser = users
                        .stream()
                        .filter(q -> q.getId() == myId)
                        .collect(Collectors.toList());

                write("SENDFILE" + "," + myId + "," + clientId + "," + f.getName() + "," + LocalDateTime.now() + "," + isGroup + "," + 0 + "," + tempUser.get(0).getFullname());
                ObjectOutputStream os1 = new ObjectOutputStream(socketOfClient.getOutputStream());
                os1.writeObject(data);
                os1.flush();
                MessageModel mess = new MessageModel(-1, myId, clientId, f.getName(), LocalDateTime.now().toString(), 0, 0, 1);
                createFileMessage(mess);
//                InputStream in1 = new FileInputStream(f);
//                OutputStream out = socketOfClient.getOutputStream();
//                
//                int count;
//                while ((count = in1.read(b)) > 0) {
//                    out.write(b, 0, count);
//                }
                System.out.println("Gui file thanh cong");
//                InputStream in1 = null;
//                OutputStream out = socketOfClient.getOutputStream();
//                int count;
//                while ((count = in1.read(b)) > 0) {
//                    out.write(b, 0, count);
//                }
//                out.writeObject(data);
//                out.flush();
//                txt.append("send 1 file ../n");
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, e, "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnFileActionPerformed

    private void btnGroupActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGroupActionPerformed
        groupF = new NewGroup(os, is, socketOfClient, myId, users);
        groupF.setVisible(true);
    }//GEN-LAST:event_btnGroupActionPerformed

    private void btnScaredActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnScaredActionPerformed
        BufferedImage myPicture;

        List<UserModel> tempUser = users
                .stream()
                .filter(q -> q.getId() == myId)
                .collect(Collectors.toList());

        try {
            myPicture = ImageIO.read(ClientF.class.getResource("/images/scared.png"));
            JLabel iconLabel = new JLabel(new ImageIcon(myPicture));

            JLabel textLabel = new JLabel();
            textLabel.setText("Ban:");

            JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            tmpPanel.add(textLabel);
            tmpPanel.add(iconLabel);

            Border border = tmpPanel.getBorder();
            Border margin = new EmptyBorder(10, 10, 10, 10);
            tmpPanel.setBorder(new CompoundBorder(border, margin));

            content.add(tmpPanel);
            content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

            jMessageScrollPanel.setViewportView(content);

            scrollToBottom();

            if (isGroup == 0) {
                write("send-to-person" + "," + myId + "," + clientId + ",3," + LocalDateTime.now() + "," + 0 + "," + 1 + "," + tempUser.get(0).getFullname());
            } else {
                write("send-to-person" + "," + myId + "," + clientId + ",3," + LocalDateTime.now() + "," + 1 + "," + 1 + "," + tempUser.get(0).getFullname());
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnScaredActionPerformed

    private void btnSadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSadActionPerformed
        BufferedImage myPicture;

        List<UserModel> tempUser = users
                .stream()
                .filter(q -> q.getId() == myId)
                .collect(Collectors.toList());

        try {
            myPicture = ImageIO.read(ClientF.class.getResource("/images/sad.png"));
            JLabel iconLabel = new JLabel(new ImageIcon(myPicture));

            JLabel textLabel = new JLabel();
            textLabel.setText("Ban:");

            JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            tmpPanel.add(textLabel);
            tmpPanel.add(iconLabel);

            Border border = tmpPanel.getBorder();
            Border margin = new EmptyBorder(10, 10, 10, 10);
            tmpPanel.setBorder(new CompoundBorder(border, margin));

            content.add(tmpPanel);
            content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

            jMessageScrollPanel.setViewportView(content);

            scrollToBottom();

            if (isGroup == 0) {
                write("send-to-person" + "," + myId + "," + clientId + ",4," + LocalDateTime.now() + "," + 0 + "," + 1 + "," + tempUser.get(0).getFullname());
            } else {
                write("send-to-person" + "," + myId + "," + clientId + ",4," + LocalDateTime.now() + "," + 1 + "," + 1 + "," + tempUser.get(0).getFullname());
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnSadActionPerformed

    private void btnHeartEyeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHeartEyeActionPerformed
        BufferedImage myPicture;

        List<UserModel> tempUser = users
                .stream()
                .filter(q -> q.getId() == myId)
                .collect(Collectors.toList());

        try {
            myPicture = ImageIO.read(ClientF.class.getResource("/images/heart_eye.png"));
            JLabel iconLabel = new JLabel(new ImageIcon(myPicture));

            JLabel textLabel = new JLabel();
            textLabel.setText("Ban:");

            JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
            tmpPanel.add(textLabel);
            tmpPanel.add(iconLabel);

            Border border = tmpPanel.getBorder();
            Border margin = new EmptyBorder(10, 10, 10, 10);
            tmpPanel.setBorder(new CompoundBorder(border, margin));

            content.add(tmpPanel);
            content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));

            jMessageScrollPanel.setViewportView(content);

            scrollToBottom();

            if (isGroup == 0) {
                write("send-to-person" + "," + myId + "," + clientId + ",5," + LocalDateTime.now() + "," + 0 + "," + 1 + "," + tempUser.get(0).getFullname());
            } else {
                write("send-to-person" + "," + myId + "," + clientId + ",5," + LocalDateTime.now() + "," + 1 + "," + 1 + "," + tempUser.get(0).getFullname());
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnHeartEyeActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws SQLException, ClassNotFoundException {
        ClientF client = new ClientF();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCrying;
    private javax.swing.JButton btnFile;
    private javax.swing.JButton btnGame;
    private javax.swing.JButton btnGroup;
    private javax.swing.JButton btnHeartEye;
    private javax.swing.JButton btnSad;
    private javax.swing.JButton btnScared;
    private javax.swing.JButton btnSend;
    private javax.swing.JButton btnSmileBig;
    private javax.swing.JButton btnSmileCry;
    private javax.swing.JPanel chatPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList<String> jListGroups;
    private javax.swing.JList<String> jListUsers;
    private javax.swing.JScrollPane jMessageScrollPanel;
    private javax.swing.JPanel jPanelGroup;
    private javax.swing.JPanel jPanelUser;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbName;
    private javax.swing.JLabel lbStatus;
    private javax.swing.JLabel recentLabel;
    private javax.swing.JPanel relativePanel;
    private javax.swing.JTextField tfInput;
    private javax.swing.JLabel txtUsername;
    // End of variables declaration//GEN-END:variables

    public JButton getBtnGame() {
        return btnGame;
    }
}
