package dao;

import connection.ConnectionUtils;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import models.MessageModel;
import models.UserModel;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Avada Kedavra
 */
public class MessageDao {

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
//        System.out.println(getMessagesOfBoxChat(-1, 16, 1).size());
//        deleteMessage("32");
        addMessage("5", "16", "tesbbb", LocalDate.now().toString(), "0", "0");
//        System.out.println(getMessagesOfBoxChat(1, 2).size());
    }

    public static List<MessageModel> getMessagesOfBoxChat(int id1, int id2, int isGroup) throws SQLException, ClassNotFoundException {
        List<MessageModel> messages = new ArrayList<>();

        // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sql = "";
        if (isGroup == 0) {
            sql = "SELECT * FROM message WHERE (senderId = " + id1 + " OR senderId = " + id2 + ") AND (receiverId = " + id2 + " OR receiverId = " + id1 + ")";
        } else {
            sql = "SELECT * FROM message WHERE receiverId = " + id2 + " AND isGroup = " + isGroup;
        }

        // Thực thi câu lệnh SQL trả về đối tượng ResultSet.
        ResultSet rs = statement.executeQuery(sql);

        // Duyệt trên kết quả trả về.
        while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
            int rsId = rs.getInt(1);
            int rsSenderId = rs.getInt(2);
            int rsReceiverId = rs.getInt(3);
            String rsText = rs.getString(4);
            String rsTimeCreated = rs.getDate(5).toString();
            int rsIsGroup = rs.getInt(6);
            int rsIsIcon = rs.getInt(7);
            int rsIsFile = rs.getInt(8);

            messages.add(new MessageModel(rsId, rsSenderId, rsReceiverId, rsText, rsTimeCreated, rsIsGroup, rsIsIcon, rsIsFile));

        }
        // Đóng kết nối
        connection.close();
        return messages;
    }

    public static void addMessage(String senderId, String receiverId, String text, String timeCreated, String isGroup, String isIcon) throws SQLException, ClassNotFoundException {
        // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sql = "INSERT INTO message VALUES (null, " + senderId + "," + receiverId + ",\"" + text + "\",\"" + timeCreated + "\"," + isGroup + "," + isIcon + ",0)";

//        String sql = "INSERT INTO message VALUES (null, 3, 4,\"test db nhaaa\", '2022-11-07', 0, 0)";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet.
        statement.executeUpdate(sql);

        // Đóng kết nối
        connection.close();
    }

    public static void addFileMessage(String senderId, String receiverId, String text, String timeCreated, String isGroup, String isIcon) throws SQLException, ClassNotFoundException {
        // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sql = "INSERT INTO message VALUES (null, " + senderId + "," + receiverId + ",\"" + text + "\",\"" + timeCreated + "\"," + isGroup + "," + isIcon + ",1)";

//        String sql = "INSERT INTO message VALUES (null, 3, 4,\"test db nhaaa\", '2022-11-07', 0, 0)";
        // Thực thi câu lệnh SQL trả về đối tượng ResultSet.
        statement.executeUpdate(sql);

        // Đóng kết nối
        connection.close();
    }

    public static void deleteMessage(String messageId) throws SQLException, ClassNotFoundException {
        // Lấy ra đối tượng Connection kết nối vào DB.
        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sql = "DELETE FROM message WHERE id = " + messageId;

        // Thực thi câu lệnh SQL trả về đối tượng ResultSet.
        statement.executeUpdate(sql);

        // Đóng kết nối
        connection.close();
    }

    public static void newGroup(String name, List<UserModel> listmember) throws SQLException, ClassNotFoundException {
        // Lấy ra đối tượng Connection kết nối vào DB.
        String timenow = "\"" + LocalDateTime.now().toString() + "\"";

        Connection connection = ConnectionUtils.getMyConnection();

        // Tạo đối tượng Statement.
        Statement statement = connection.createStatement();

        String sql = "INSERT INTO room VALUES (null,'" + name + "',1)";
        statement.executeUpdate(sql);
        String sql2 = "Select idroom from room where name = ?";
        PreparedStatement stmt = connection.prepareStatement(sql2);
        stmt.setString(1, name);
        ResultSet rs = stmt.executeQuery();
        int idroom = 0;
        // Duyệt trên kết quả trả về.
        while (rs.next()) {// Di chuyển con trỏ xuống bản ghi kế tiếp.
            idroom = rs.getInt(1);
        }
        for (UserModel x : listmember) {

            String sql3 = "INSERT INTO participants VALUES(null," + x.getId() + "," + idroom + "," + timenow + ")";
            statement.executeUpdate(sql3);
        }
        // Đóng kết nối
        connection.close();
    }
}
