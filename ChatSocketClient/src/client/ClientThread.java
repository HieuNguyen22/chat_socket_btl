/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package client;

import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import models.Data;
import models.Groups;
import models.MessageModel;
import models.Messages;
import models.UserModel;
import models.Users;

/**
 *
 * @author Avada Kedavra
 */
public class ClientThread implements Runnable {

    Socket socket;
    BufferedWriter os;
    BufferedReader is;
    Login main;
    StringTokenizer st;
    private boolean isConnected = false;

    private int clientId;
    protected DecimalFormat df = new DecimalFormat("##,#00");

    private String clientPath = "D:/files_btl_ltm/file_client/";

    public ClientThread(Socket socket, Login main) {
        this.main = main;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            // Gửi yêu cầu kết nối tới Server đang lắng nghe
            // trên máy 'localhost' cổng 7777.
//            socket = new Socket("localhost", 7777);
            System.out.println("Kết nối thành công!");
            // Tạo luồng đầu ra tại client (Gửi dữ liệu tới server)
            os = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            // Luồng đầu vào tại Client (Nhận dữ liệu từ server).
            is = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String message;
            while (true) {
                message = is.readLine();
                System.out.println(message);
                if (message == null) {
                    System.out.println("OK");

                }
                String[] messageSplit = message.split(",");
                if (messageSplit[0].equals("get-id")) {
                    main.setID(Integer.parseInt(messageSplit[1]));
                    main.setIDTitle();
                }
                if (messageSplit[0].equals("login ok")) {
                    main.setVisible(false);

                    // Get users
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    System.out.println("da tao luong nhan");
                    Users allfriends = (Users) ois.readObject();

                    System.out.println(allfriends.getUsers().size());
                    System.out.println("da nhan duoc object");

                    main.client = new ClientF();
                    main.client.setSocket(socket, os, is);
                    main.client.setID(main.getMyId());
                    main.client.clientController = main.clientController;
                    main.client.clientController.setFrame(main.client);
                    main.client.users = allfriends.getUsers();
                    main.client.setUsername(main.getName());
                    main.client.catchEventOnline();
                    main.client.catchEventGroup();

                    main.client.write("request-groups," + main.getMyId());
                }

                if (messageSplit[0].equals("new-group-ok")) {
                    main.client.write("request-groups," + main.getMyId());
                }

                if (messageSplit[0].equals("send-groups")) {
                    // Get groups
                    ObjectInputStream oisGroup = new ObjectInputStream(socket.getInputStream());

                    Groups allGroups = (Groups) oisGroup.readObject();

                    main.client.groups.clear();
                    main.client.groups.addAll(allGroups.getGroups());
                    System.out.println("DA NHAN GROUPS: " + main.client.groups.size());

                    main.client.setDefaultValues();
                    main.client.setVisible(true);
                }

                if (messageSplit[0].equals("login failed")) {
//                                 JOptionPane.showMessageDialog(this, "Login failed","Thông báo",JOptionPane.OK_OPTION);
                }
                if (messageSplit[0].equals("sign up failed")) {

                    System.out.println("dang ky loi");
                }

                if (messageSplit[0].equals("allfriends")) {
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    List<UserModel> allfriends = (List<UserModel>) ois.readObject();
                    main.client.users = allfriends;
                    System.out.println("nhan object okie");
                }

                if (messageSplit[0].equals("person-message")) {
                    System.out.println("DA NHAN DUOC TIN NHAN NHOM");

                    JLabel text = new JLabel();
                    text.setText(messageSplit[1]);

                    JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
                    tmpPanel.add(text);

                    Border border = tmpPanel.getBorder();
                    Border margin = new EmptyBorder(10, 10, 10, 10);
                    tmpPanel.setBorder(new CompoundBorder(border, margin));

                    main.client.content.add(tmpPanel);
                    main.client.content.setLayout(new BoxLayout(main.client.content, BoxLayout.Y_AXIS));

                    main.client.jMessageScrollPanelsetViewportView(main.client.content);
                    main.client.scrollToBottom();

                }

                if (messageSplit[0].equals("person-icon")) {
                    main.client.createIconMessage(Integer.parseInt(messageSplit[1]), Integer.parseInt(messageSplit[2]));
                    main.client.scrollToBottom();
                }

                if (messageSplit[0].equals("get-file")) {
                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                    Data data = (Data) ois.readObject();
                    File myObj = new File(clientPath + data.getName());

                    myObj.createNewFile();
                    OutputStream out = new FileOutputStream(clientPath + data.getName());
                    out.write(data.getFile());
                    out.close();
                }
                if (messageSplit[0].equals("person-file")) {
                    MessageModel mess = new MessageModel(-1, Integer.parseInt(messageSplit[3]), main.getMyId(), messageSplit[1], LocalDateTime.now().toString(), 0, 0, 1);
                    main.client.createFileMessage(mess);
//
//                    JButton text = new JButton();
//                    text.setText(messageSplit[1]);
//
//                    JPanel tmpPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
//                    tmpPanel.add(text);
//
//                    Border border = tmpPanel.getBorder();
//                    Border margin = new EmptyBorder(10, 10, 10, 10);
//                    tmpPanel.setBorder(new CompoundBorder(border, margin));
//
//                    main.client.content.add(tmpPanel);
//                    main.client.content.setLayout(new BoxLayout(main.client.content, BoxLayout.Y_AXIS));
//
//                    main.client.jMessageScrollPanelsetViewportView(main.client.content);
//                    main.client.scrollToBottom();
//
//                    ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
//                    Data data = (Data) ois.readObject();
//                    File myObj = new File(clientPath + data.getName());
//
//                    myObj.createNewFile();
//                    OutputStream out = new FileOutputStream(clientPath + data.getName());
//                    out.write(data.getFile());
//                    out.close();
//
//                    text.addActionListener((ActionListener) new java.awt.event.ActionListener() {
//                        @Override
//                        public void actionPerformed(ActionEvent e) {
//                            Desktop d = null;
//                            if (Desktop.isDesktopSupported()) {
//                                System.out.println("Co sp");
//                                File f = new File(clientPath + data.getName());
//                                if (!f.exists()) {
//                                    try {
//                                        System.out.println("Getfile demo");
//                                        os.write("Get File," + data.getName());
//                                        os.newLine();
//                                        os.flush();
//                                    } catch (IOException ex) {
//                                        Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
//                                    }
//
//                                }
//                                try {
//                                    Desktop.getDesktop().open(f);
//                                } catch (IOException ex) {
//                                    Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
//                                }
//                            }
//
//                        }
//
//                    });
                }

                if (messageSplit[0].equals("person-filegroup") && main.client.isGroup == 1) {
                    MessageModel mess = new MessageModel(-1, Integer.parseInt(messageSplit[3]), main.client.getMyId(), messageSplit[1], LocalDateTime.now().toString(), 1, 0, 1);
                    main.client.createFileMessage(mess);
                }

                if (messageSplit[0].equals("refresh-messages")) {
                    try {
                        if (messageSplit[1].equals("0")) {
                            os.write("getmessage," + main.getMyId() + "," + main.client.clientId + ",0");
                            os.newLine();
                            os.flush();
                        } else {
                            os.write("getmessage," + main.getMyId() + "," + main.client.clientId + ",1");
                            os.newLine();
                            os.flush();
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ClientF.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                try {
                    if (messageSplit[0].equals("message")) {
                        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                        Messages k = (Messages) ois.readObject();

                        main.client.messages.clear();
                        main.client.messages.addAll(k.getListmessages());
                        main.client.refreshMessages();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

//                            if (messageSplit[0].equals("update-online-list")) {
//                                onlineList = new ArrayList<>();
//                                String online = "";
//                                String[] onlineSplit = messageSplit[1].split("-");
//                                for (int i = 0; i < onlineSplit.length; i++) {
//                                    onlineList.add(onlineSplit[i]);
//                                    online += "Client " + onlineSplit[i] + " đang online\n";
//                                }
//                                taOnline.setText(online);
//                                //updateCombobox();
//                            }
//                            if (messageSplit[0].equals("global-message")) {
//                                taMessage.setText(taMessage.getText() + messageSplit[1] + "\n");
//                                taMessage.setCaretPosition(taMessage.getDocument().getLength());
//                            }
            }
//                    os.close();
//                    is.close();
//                    socketOfClient.close();
        } catch (UnknownHostException e) {
            return;
        } catch (IOException e) {
            return;
        } catch (SQLException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Login.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
